/* mainpage.h  --  Doxygen main page - only to be used by doxygen
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @mainpage eurephia v1.0 developers reference
 * @section overview Overview of the source tree
 * eurephia is built up in a modular way and code reuse is an important element.  The source
 * tree is divided into several smaller parts where each part or module is located.
 *
 * The project is written strictly in C, but the administration utility makes use of XSLT
 * as a template engine to display reports.
 *
 * - <b>common/</b>
 *   - This is where all source code used by all modules should be expected to reside
 *
 * - <b>database/</b>
 *   - Everything connected to the database API is found here.  The generic database modules
 *     will also be located in the root of this directory.  Each database driver must reside
 *     in a separate directory
 *
 * - <b>database/sqlite/</b>
 *   - The SQLite3 database driver (edb-sqlite.so) is located here
 *   - @ref edb-sqlite.c
 *
 * - <b>eurephiadm/</b>
 *   - Everything connected to the eurephiadm, the console based administration utility.
 *   - @ref eurephiadm.c
 *
 * - <b>eurephia/command/</b>
 *   - eurephiadm is built up by a set of commands.  Each command is found in separate
 *     files here.
 *
 * - <b>plugin/</b>
 *   - The OpenVPN plug-in (eurephia-auth.so) is located in this directory
 *   - @ref eurephia-auth.c
 *
 * - <b>plugin/firewall/</b>
 *   - The generic parts for the firewall integration of the plug-in
 *
 * - <b>plugin/firewall/iptables/</b>
 *       - The iptables "driver" (efw-iptables.so) for the firewall integration
 *       - @ref efw-iptables.c
 *
 * - <b>utils/</b>
 *       - Utilities which is not installed during make install.  The eurephia_init program
 *         resides here together with saltdecode.  eurephia_init is used to configure a new
 *         eurephia database, while saltdecode is used for debugging and will decode
 *         the password salt hashing information.
 *       - @ref eurephia_init.c eurephia_saltdecode.c
 *
 * <hr>
 * @section build Building eurephia and installing
 * Run the <i>./configure</i> script to prepare the build.  This script requires
 * <a href="http://www.cmake.org">CMake</a> (at least version 2.6) to be installed.  The configure
 * script will require some parameters, depending on which features being prepared in the build.
 * For a complete overview of all parameters, see <i>./configure --help</i>
 *
 * Then run <i>make</i> to do the build.
 *
 * To install eurephia,  Run <i>make install</i>.
 * <hr>
 * @section patches How to contribute
 * Contributions to the source code will be very much appreciated.  Patches should be based
 * on the master branch in the git tree.  Patches should be uploaded to the project web site
 * on sourceforge.net
 *
 *       http://sourceforge.net/tracker/?group_id=236344&atid=1099762
 *
 * Contributed patches should be prepared by using the
 * <a href="http://www.kernel.org/pub/software/scm/git/docs/git-format-patch.html">git format-patch</a>
 * command.
 *
 * <hr>
 * @section locations Locations
 * - <b>Website</b>
 *   - http://www.eurephia.net/
 *
 * - <b>Community places</b>
 *   - http://sourceforge.net/projects/eurephia
 *   - IRC: irc.freenode.net, @#eurephia
 *
 * - <b>Trackers</b>
 *   - Bugs: http://sourceforge.net/tracker/?group_id=236344&atid=1099760
 *   - Patches: http://sourceforge.net/tracker/?group_id=236344&atid=1099762
 *
 * - <b>Source code</b>
 *   - git repository: git://eurephia.git.sourceforge.net/gitroot/eurephia/eurephia
 *   - Browse the source code: http://eurephia.git.sourceforge.net/git/gitweb-index.cgi
 *
 * <hr>
 * @section contact Contact
 *   - David Sommerseth (irc: dazo) <dazo@users.sourceforge.net>
 *
 * <hr>
 * @section requirements Requirements
 *
 * Generic requirements for building eurephia
 *   - <a href="http://www.cmake.org/">CMake</a>
 *
 * The following header files and libraries is required to build eurephia:
 *
 * - <b>eurephia-auth</b> - OpenVPN authentication plug-in
 *   - <a href="http://www.sqlite.org">SQLite3</a>
 *   - <a href="http://www.openvpn.net/index.php/open-source/downloads.html">OpenVPN source tree</a>
 *
 * - <b>eurephia administration utility</b> (eurephiadm and eurephia_init)
 *   - <a href="http://www.sqlite.org">SQLite3</a>
 *   - <a href="http://www.xmlsoft.org/">libxml2</a>
 *   - <a href="http://xmlsoft.org/XSLT/">libxslt2</a>
 *
 */
