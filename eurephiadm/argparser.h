/* argparser.c  --  Simple argument parser - inspired by getopt, but simpler
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   argparser.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2008-12-04
 *
 * @brief  A simple argument parser, inspired by getopt.
 *
 */

#ifndef EUREPHIA__ARGPARSE_H
#define EUREPHIA__ARGPARSE_H

#ifndef ARGPARSER_C
# ifndef MODULE
#  error To use the argparser, you must define MODULE (string)
# endif
#endif

#define MAX_ARGUMENTS 64        /**< Defines the maximum arguments the parsers handles */

/**
 * Contains the valid short and long argument definition, in addition to number of options
 * the current argument takes.
 */
typedef struct {
        char *longarg;          /**< char* containing the valid long argument */
        char *shortarg;         /**< char* containing the short argument */
        int param;              /**< int containing the number of parameters this argument takes */
} e_options;                    /**< The long and short arguments must contain the leading '-' character(s) */

extern char *optargs[MAX_ARGUMENTS]; /**< Contains the parameters of an argument, if param > 0 */

int _eurephia_getopt(const char *module, int *curarg, int argc, char **argv, e_options *argopts);
/**
 * Parses an argument and prepares the char **optargs array with parameters to the arguments.
 * Macro for the internal _eurephia_getopt(), which automatically sets the const char *module
 * parameter.
 *
 * @param ca    pointer to the argument index counter which gets updated during parsing.
 * @param ac    Input argument counter
 * @param av    Input argument array (char **)
 * @param opts  e_options struct array containing valid argument
 *
 * @return returns the short arg value on success, or -1 on failure.
 */
#define eurephia_getopt(ca, ac, av, opts) _eurephia_getopt(MODULE, ca, ac, av, opts)

size_t eurephia_arraycp(int stidx, int inargc, char **inargv, char **outargv, size_t outsize);
#endif
