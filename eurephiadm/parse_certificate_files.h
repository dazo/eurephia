/* parse_certificate_files.c  --  Parses PEM or PKCS12 formatted cert. files
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   parse_certificate_files.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2008-12-21
 *
 * @brief  Parses PEM/DER or PKCS#12 certificate files to retrieve information
 *         needed by eurephia.  This feature requires OpenSSL to be available.
 *
 */

#ifndef   	PARSE_CERTIFICATE_FILES_H_
# define   	PARSE_CERTIFICATE_FILES_H_

#include <certinfo.h>

#ifdef HAVE_OPENSSL
#define CERTFILE_PEM    0x01    /**< Input certificate is in PEM/DER format */
#define CERTFILE_PKCS12 0x02    /**< Input certificate is in PKCS#12 format */

#ifndef _PARSE_CERTFICIATE_FILES_C

/**
 * Macro to _Cert_ParseFile(...).  Parses a certificate file of a given format.
 *
 * @param cfile    File name of the input file
 * @param cformat  Certificate format of the file
 *
 * @return Returns a certinfo struct pointer with the parsed result on success, otherwise NULL.
 */
#define Cert_ParseFile(cfile,cformat) _Cert_ParseFile(MODULE, cfile, cformat)
certinfo *_Cert_ParseFile(const char *module, const char *certfile, int certfile_format);

#endif      /* !_PARSE_CERTIFICATE_FILES_C */

#endif      /* HAVE OPENSSL */
#endif 	    /* !PARSE_CERTIFICATE_FILES_H_ */
