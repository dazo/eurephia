/* certificates.c  --  eurephiadm command: certs 
 *                     Certificate management
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   eurephiadm/commands/certificates.c
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2008-12-20
 *
 * @brief  eurephiadm certs command, for managing the eurephia certificates.
 *
 */


#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include <openssl/x509.h>

#ifdef HAVE_LIBXML2
#include <libxml/tree.h>
#include <libxml/xpath.h>
#endif

#define MODULE "eurephia::Certificates" /**< Need to define the active module before including argparser.h */
#include <eurephia_nullsafe.h>
#include <eurephia_context.h>
#include <eurephia_log.h>
#include <eurephia_xml.h>
#include <eurephia_values_struct.h>
#include <eurephiadb_session_struct.h>
#include <eurephiadb_mapping.h>
#include <eurephiadb_driver.h>
#include <certinfo.h>

#include "../argparser.h"
#include "../get_console_input.h"
#include "../parse_certificate_files.h"
#include "../xsltparser.h"


/**
 * Help screens for the certs command
 *
 * @param page which help screen to display
 */
void display_certs_help(int page) {
        switch( page ) {
        case 'A':
                printf("The add mode will register a new certificate.\n\n"
                       "     -d | --depth         Certificate depth, required.\n"
                       "     -D | --digest        SHA1 fingerprint/digest of the new certificate\n"
                       "     -C | --common-name   Common name (CN) field of the certificate\n"
                       "     -O | --organisation  Organisation (O) field of the certificate\n"
                       "     -E | --email         e-mail address (emailAddress) of the certificate\n"
                       "\n"
                       "Usually the certificate depth value needs to be 0, if you are registering user\n"
                       "account certificates. CA certificates usually have a value bigger than 0.\n\n");
#ifdef HAVE_OPENSSL
                printf("If you have the certificate file available, you can use the following\n"
                       "options to retrieve the needed information directly from a certificate file.\n\n"
                       "     -f | --certfile     File name of the certificate file.\n"
                       "     -p | --pkcs12       If the file is in PKCS#12 format.\n"
                       "\n"
                       "The default format is PEM format, unless --pkcs12 is given.  These two options\n"
                       "cannot be used together with -D, -C, -O or -E.  But the certificate depth must\n"
                       "be given to indicate the certificate depth.\n\n");
#endif
               break;
        case 'D':
                printf("The delete mode will remove a certificate from the certificate database.\n\n"
                       "      -i | --certid        Indicates a unique certificate ID\n"
                       "      -d | --digest        A unique SHA1 fingerprint/digest value\n"
                       "      -C | --common-name   Common Name (CN) field of a certificate\n"
                       "      -O | --organisation  Organisation (O) field of a certificate\n"
                       "      -E | --email         e-mail address (emailAddress) of a certificate\n"
                       "\n"
                       "You can use any of these parameters to indicate a search criteria for the\n"
                       "certificate (or certificates) you want to delete.  You will be provided with\n"
                       "a list over certificates which matches your search criteria and you will need\n"
                       "to approve the deletion of the matching certificate(s).\n\n");
                break;

        case 'l':
                printf("The list mode will list all registered certificates.  It accepts one parameter:\n\n"
                       "     -S | --sort <sort key>     Decide the sort order of the certificate list\n"
                       "\n"
                       "Available sort keys are: certid, depth, digest, cname, org, email and registered.\n\n");
                break;
       default:
                printf("Available modes for the certificate command are:\n\n"
                       "     -A | --add         Register a new certificate\n"
                       "     -D | --delete      Delete a registered certificate\n"
                       "     -l | --list        List all registered certificates\n"
                       "     -h | --help <mode> Help about a specific mode\n\n");
                break;
        }
}


/**
 * Help screen wrapper.  Used by cmd_Help()
 */
void help_Certificates() {
        display_certs_help(0);
}


/**
 * Help screen wrapper for the certs help function.
 *
 * @param ctx   eurephiaCTX
 * @param sess  eurephiaSESSION of the current logged in user
 * @param cfg   eurephiaVALUES struct of the current configuration
 * @param argc  argument count for the eurephiadm command
 * @param argv  argument table for the eurephiadm command
 *
 * @return returns 0 on success, otherwise 1.
 */
int help_Certificates2(eurephiaCTX *ctx, eurephiaSESSION *sess, eurephiaVALUES *cfg, int argc, char **argv) {
        e_options helpargs[] = {
                {"--list", "-l", 0},
                {"--add", "-A", 0},
                {"--delete", "-D", 0},
                {NULL, NULL, 0}
        };

        int i = 1;
        display_certs_help(eurephia_getopt(&i, argc, argv, helpargs));
        return 0;
}

/**
 * Internal certs command, does the proper register request against the eurephia database.
 *
 * @param ctx    eurephiaCTX
 * @param depth  Certificate depth of the certificate
 * @param digest Certificate SHA1 fingerprint (digest)
 * @param cname  X.509 Common Name value of the certificate
 * @param org    X.509 Organisation value of the certificate
 * @param email  X.509 emailAddress value of the certificate
 *
 * @return Returns the certid reference to the certificate on successful registration, otherwise 0.
 */
int register_certificate(eurephiaCTX *ctx, int depth, const char *digest,
                  const char *cname, const char *org, const char *email)
{
        xmlDoc *cert_xml = NULL, *res_xml = NULL;
        xmlNode *cert_n = NULL;
        eurephiaRESULT *res = NULL;
        char tmp[66], *cname_cp = NULL, *org_cp = NULL;
        int certid = 0;

        assert( ctx != NULL );

        eurephiaXML_CreateDoc(ctx, 1, "certificates", &cert_xml, &cert_n);
        assert( (cert_xml != NULL) || (cert_n != NULL) );
        xmlNewProp(cert_n, (xmlChar *) "mode", (xmlChar *) "register");

        cert_n = xmlNewChild(cert_n, NULL, (xmlChar *) "fieldMapping", NULL);
        xmlNewProp(cert_n, (xmlChar *) "table", (xmlChar *) "certificates");

        memset(&tmp, 0, 66);
        snprintf(tmp, 64, "%i", depth);

        cname_cp = strdup_nullsafe(cname);
        org_cp   = strdup_nullsafe(org);

        xmlNewChild(cert_n, NULL, (xmlChar *) "depth", (xmlChar *) tmp);
        xmlNewChild(cert_n, NULL, (xmlChar *) "digest", (xmlChar *) digest);
        xmlNewChild(cert_n, NULL, (xmlChar *) "cname", (xmlChar *) cname_cp);
        xmlNewChild(cert_n, NULL, (xmlChar *) "org", (xmlChar *) org_cp);
        xmlNewChild(cert_n, NULL, (xmlChar *) "email", (xmlChar *) email);

        // Register the certificate
        res_xml = eDBadminCertificate(ctx, cert_xml);
        if( res_xml == NULL ) {
                fprintf(stderr, "%s: Failed to register certificate\n", MODULE);
        } else {
                res = eurephiaXML_ParseResultMsg(ctx, res_xml);
                if( res == NULL ) {
                        fprintf(stderr, "%s: Failed to register certificate. No results available\n", MODULE);
                        certid = 0;
                } else if( res->resultType == exmlRESULT ) {
                        cert_n = xmlFindNode(res->details, "certificate");
                        if( cert_n == NULL ) {
                                fprintf(stderr, "%s: Did not receive certificate ID of the newly registered"
                                        " certificate\n", MODULE);
                                certid = 0;
                        } else {
                                certid = atoi_nullsafe(xmlGetAttrValue(cert_n->properties, "certid"));
                                fprintf(stdout, "%s: %s\n", MODULE, res->message);
                        }
                } else {
                        fprintf(stderr, "%s: %s\n", MODULE, res->message);
                        certid = 0;
                }
                free_nullsafe(ctx, res);
                xmlFreeDoc(res_xml);
        }
        xmlFreeDoc(cert_xml);
        free_nullsafe(ctx, cname_cp);
        free_nullsafe(ctx, org_cp);

        return certid;
}


/**
 * certs add mode.  Registers a new certificate.
 *
 * @param ctx   eurephiaCTX
 * @param sess  eurephiaSESSION of the current logged in user
 * @param cfg   eurephiaVALUES struct of the current configuration
 * @param argc  argument count for the eurephiadm command
 * @param argv  argument table for the eurephiadm command
 *
 * @return returns 0 on success, otherwise 1.
 */
int add_cert(eurephiaCTX *ctx, eurephiaSESSION *sess, eurephiaVALUES *cfg, int argc, char **argv) {
        char *digest = NULL, *cname = NULL, *org = NULL, *email = NULL, *certfile = NULL;
        int depth = -1, i = 0, j = 0, chk = 0, certfile_format = 0, rc = 0;
#ifdef HAVE_OPENSSL
        struct stat cert_stat;
#endif

        e_options addcertargs[] = {
                {"--depth",           "-d", 1},
                {"--digest",          "-D", 1},
                {"--common-name",     "-C", 1},
                {"--organisation",    "-O", 1},
                {"--email",           "-E", 1},
#ifdef HAVE_OPENSSL
                {"--certfile",        "-f", 1},
                {"--pkcs12",          "-p", 0},
#endif
                {"--help",            "-h", 0},
                {NULL, NULL, 0}
        };

        certfile = NULL;
        certfile_format = CERTFILE_PEM;  // Default file format when not specified
        for( i = 1; i < argc ; i++ ) {
                switch( eurephia_getopt(&i, argc, argv, addcertargs) ) {
                case 'd': // Certificate depth

                        // Check if argument is a number
                        chk = 1;
                        for( j = 0; j < strlen_nullsafe(optargs[0]); j++ ) {
                                if( !isdigit(optargs[0][j]) ) {
                                        chk = 0;
                                        break;
                                }
                        }
                        if( chk == 0 )  {
                                fprintf(stderr, "%s: Certificate depth must be a number\n", MODULE);
                                rc = 1;
                                goto exit;
                        }

                        depth = atoi_nullsafe(optargs[0]);
                        if( (depth < 0) || (depth > 99) ) {
                                fprintf(stderr, "%s: Certificate depth must be between 0-99\n", MODULE);
                                rc = 1;
                                goto exit;
                        }
                        break;

                case 'D': // Certificate digest
                        if( strlen_nullsafe(optargs[0]) < 59 ) {
                                fprintf(stderr, "%s: Certificate digest is too short\n", MODULE);
                                rc = 1;
                                goto exit;
                        }
                        digest = strdup_nullsafe(optargs[0]);
                        break;
                case 'C': // Certificate - common_name / CN
                        cname = strdup_nullsafe(optargs[0]);
                        break;
                case 'O': // Certificate - organisation / O
                        org = strdup_nullsafe(optargs[0]);
                        break;
                case 'E': // Certificate - e-mail / emailAddr
                        email = strdup_nullsafe(optargs[0]);
                        break;

#ifdef HAVE_OPENSSL
                case 'p': // Certfile is in PKCS#12 format
                        certfile_format = CERTFILE_PKCS12;
                        break;

                case 'f': // Load certificate info from a certificate file
                        if( strlen_nullsafe(optargs[0]) < 1 ) {
                                fprintf(stderr, "%s: certfile is too short\n", MODULE);
                                rc = 1;
                                goto exit;
                        }
                        certfile = optargs[0];

                        if( stat(certfile, &cert_stat) == -1 ) {
                                fprintf(stderr, "%s: Could not access certfile: %s (%s)\n", MODULE,
                                        certfile, strerror(errno));
                                rc = 1;
                                goto exit;
                        }

                        if( cert_stat.st_size == 0 ) {
                                fprintf(stderr, "%s: certfile '%s' is empty\n", MODULE, certfile);
                                rc = 1;
                                goto exit;
                        }
                        break;
#endif
                case 'h':
                        display_certs_help('A');
                        rc = 0;
                        goto exit;

                default:
                        rc = 1;
                        goto exit;
                }
        }

        // Sanity check of input parameters
        if( depth < 0 ) {
                fprintf(stderr, "%s: You must set certificate depth (possibly, it needs to be 0)\n", MODULE);
                rc = 1;
                goto exit;
        }

        if( (certfile != NULL) && ((digest != NULL) || (cname != NULL) || (org != NULL) || (email != NULL)) ){
                fprintf(stderr,
                        "%s: You cannot combine --certfile with --depth, --common-name,\n"
                        "--organisation or --email\n", MODULE);
                rc = 1;
                goto exit;
        }

        if( (certfile == NULL) && ((digest == NULL) || (cname == NULL) || (org == NULL) || (email == NULL)) ) {
                fprintf(stderr,
                        "%s: You must use either --certfile or --depth, --common-name,\n"
                        "--organisation and --email\n", MODULE);
                rc = 1;
                goto exit;
        }

#ifdef HAVE_OPENSSL
        // If we have a certfile - open it and fetch the info we want
        if( certfile != NULL ) {
                certinfo *ci = NULL;
                if( (ci = Cert_ParseFile(certfile, certfile_format)) == NULL ) {
                        fprintf(stderr, "%s: Failed to parse certificate file\n", MODULE);
                        rc = 1;
                        goto exit;
                }
                digest = strdup_nullsafe(ci->digest);
                cname  = strdup_nullsafe(ci->common_name);
                org    = strdup_nullsafe(ci->org);
                email  = strdup_nullsafe(ci->email);
                free_certinfo(ci); ci = NULL;

        }
#endif
        // register_certificate returns value < 1, it means registration failed => rc = 1
        rc = (register_certificate(ctx, depth, digest, cname, org, email) < 1);
 exit:
        free_nullsafe(ctx, digest);
        free_nullsafe(ctx, cname);
        free_nullsafe(ctx, org);
        free_nullsafe(ctx, email);
        return rc;
}


/**
 * certs delete mode.  Removes a registered certificate from the eurephia database.
 *
 * @param ctx   eurephiaCTX
 * @param sess  eurephiaSESSION of the current logged in user
 * @param cfg   eurephiaVALUES struct of the current configuration
 * @param argc  argument count for the eurephiadm command
 * @param argv  argument table for the eurephiadm command
 *
 * @return returns 0 on success, otherwise 1.
 */
int delete_cert(eurephiaCTX *ctx, eurephiaSESSION *sess, eurephiaVALUES *cfg, int argc, char **argv) {
        xmlDoc *cert_xml = NULL, *delete_xml = NULL, *certlist = NULL, *res_xml = NULL;
        xmlNode *cert_n = NULL, *search_n = NULL, *delete_n = NULL;
        eurephiaRESULT *res = NULL;
        char *digest = NULL, *cname = NULL, *org = NULL, *email = NULL, *certid = NULL, confirm[5];
        const char *xsltparams[] = {"view_digest", "'1'", "firewall", "'0'", NULL};
        int i, rc = 0, count = 0;
        e_options addcertargs[] = {
                {"--certid",          "-i", 1},
                {"--digest",          "-d", 1},
                {"--common-name",     "-C", 1},
                {"--organisation",    "-O", 1},
                {"--email",           "-E", 1},
                {"--help",            "-h", 0},
                {NULL, NULL, 0}
        };

        for( i = 1; i < argc ; i++ ) {
                switch( eurephia_getopt(&i, argc, argv, addcertargs) ) {
                case 'i':
                        if( atoi_nullsafe(optargs[0]) < 1 ) {
                                fprintf(stderr, "%s: Certificate ID (certid) must be > 0\n", MODULE);
                                rc = 1;
                                goto exit;
                        }
                        certid = optargs[0];
                        break;
                case 'd': // Certificate digest
                        if( strlen_nullsafe(optargs[0]) < 59 ) {
                                fprintf(stderr, "%s: Certificate digest is too short\n", MODULE);
                                rc = 1;
                                goto exit;
                        }
                        digest = optargs[0];
                        break;
                case 'C': // Certificate - common_name / CN
                        cname = strdup_nullsafe(optargs[0]);
                        break;
                case 'O': // Certificate - organisation / O
                        org = strdup_nullsafe(optargs[0]);
                        break;
                case 'E': // Certificate - e-mail / emailAddr
                        email = optargs[0];
                        break;
                case 'h':
                        display_certs_help('D');
                        rc = 0;
                        goto exit;
                default:
                        rc = 1;
                        goto exit;
                }
        }

        // Sanity check
        if( (certid == NULL) && (digest == NULL) && (cname == NULL) && (org == NULL) && (email == NULL) ) {
                fprintf(stderr,
                        "%s: You must add at least one search criteria to delete a certificate\n",
                        MODULE);
                rc = 1;
                goto exit;
        }

        // Create a field mapping containing our search criteria(s)
        eurephiaXML_CreateDoc(ctx, 1, "certificates", &cert_xml, &cert_n);
        assert( (cert_xml != NULL) && (cert_n != NULL));
        xmlNewProp(cert_n, (xmlChar *) "mode", (xmlChar *) "list");
        xmlNewChild(cert_n, NULL, (xmlChar *) "sortkeys", (xmlChar *) "certid");

        search_n = xmlNewChild(cert_n, NULL, (xmlChar *) "fieldMapping", NULL);
        xmlNewProp(search_n, (xmlChar *) "table", (xmlChar *) "certificates");

        if(  certid != NULL ) {
                xmlNewChild(search_n, NULL, (xmlChar *) "certid", (xmlChar *) certid);
        }
        if( digest != NULL ) {
                xmlNewChild(search_n, NULL, (xmlChar *) "digest", (xmlChar *) digest);
        }
        if(  cname != NULL ) {
                xmlNewChild(search_n, NULL, (xmlChar *) "cname", (xmlChar *) cname);
        }
        if(  org!= NULL ) {
                xmlNewChild(search_n, NULL, (xmlChar *) "org", (xmlChar *) org);
        }
        if(  email != NULL ) {
                xmlNewChild(search_n, NULL, (xmlChar *) "email", (xmlChar *) email);
        }

        // Look up the certificate info and display it
        certlist = eDBadminCertificate(ctx, cert_xml);
        if( certlist == NULL ) {
                rc = 0;
                goto exit;
        }

        delete_n = eurephiaXML_getRoot(ctx, certlist, "certificates", 1);
        if( atoi_nullsafe(xmlGetAttrValue(delete_n->properties, "certificates")) == 0 ) {
                printf("%s: No certificates found\n", MODULE);
                rc = 0;
                goto exit;
        }

        // Show certificate(s)
        xslt_print_xmldoc(stdout, cfg, certlist, "certificates.xsl", xsltparams);

        // Get delete confirmation
        memset(&confirm, 0, 5);
        printf("\n");
        get_console_input(confirm, 3, "Do you want to delete the listed certificate(s)? [y/N] ", 0);
        if( (confirm[0] != 'y') && (confirm[0] != 'Y') ) {
                printf("%s: Aborted certificate deletion\n", MODULE);
                rc = 0;
                goto exit;
        }

        // If user accepts to delete certificate(s), do so
        eurephiaXML_CreateDoc(ctx, 1, "certificates", &delete_xml, &delete_n);
        assert( (delete_xml != NULL) && (delete_n != NULL));
        xmlNewProp(delete_n, (xmlChar *) "mode", (xmlChar *) "delete");
        xmlAddChild(delete_n, xmlCopyNode(search_n, 1));

        res_xml = eDBadminCertificate(ctx, delete_xml);
        if(  res_xml == NULL ) {
                fprintf(stderr, "%s: Failed to delete the certificate%s\n", MODULE, (count != 1 ? "s" : ""));
                rc = 1;
        } else {
                res = eurephiaXML_ParseResultMsg(ctx, res_xml);
                if( res == NULL ) {
                        fprintf(stderr, "%s: Failed to delete the certificate%s\n", MODULE,
                                (count != 1 ? "s" : ""));
                        rc = 1;
                } else if( res->resultType == exmlERROR ) {
                        fprintf(stderr, "%s: %s\n", MODULE, res->message);
                        rc = 1;
                } else {
                        fprintf(stdout, "%s: %s\n", MODULE, res->message);
                        rc = 0;
                }
                free_nullsafe(ctx, res);
                xmlFreeDoc(res_xml);
        }
        xmlFreeDoc(delete_xml);

 exit:
        xmlFreeDoc(certlist);
        xmlFreeDoc(cert_xml);
        free_nullsafe(ctx, cname);
        free_nullsafe(ctx, org);
        return rc;
}


/**
 * certs list mode.  Lists all registered certificate.
 *
 * @param ctx   eurephiaCTX
 * @param sess  eurephiaSESSION of the current logged in user
 * @param cfg   eurephiaVALUES struct of the current configuration
 * @param argc  argument count for the eurephiadm command
 * @param argv  argument table for the eurephiadm command
 *
 * @return returns 0 on success, otherwise 1.
 */
int list_certs(eurephiaCTX *ctx, eurephiaSESSION *sess, eurephiaVALUES *cfg, int argc, char **argv) {
        xmlDoc *srch_xml = NULL, *certlist = NULL;
        xmlNode *cert_n = NULL, *srch_n = NULL;
        const char *xsltparams[] = {"view_digest", "'1'", "firewall", "'0'", NULL};
        int i, rc = 0;
        char *sortkeys = NULL;

        e_options listargs[] = {
                {"--sort", "-S", 1},
                {"--help", "-h", 0},
                {NULL, NULL, 0}
        };

        // Parse arguments
        for( i = 1; i < argc; i++ ) {
                switch( eurephia_getopt(&i, argc, argv, listargs) ) {
                case 'S':
                        sortkeys = optargs[0];
                        i++;
                        break;

                case 'h':
                        display_certs_help('l');
                        return 0;

                default:
                        return 1;
                }
        }

        if( sortkeys == NULL ) {
                sortkeys = "certid";
        }

        // Create an empty field mapping to get all certificates
        eurephiaXML_CreateDoc(ctx, 1, "certificates", &srch_xml, &srch_n);
        assert( (srch_xml != NULL) && (srch_n != NULL));
        xmlNewProp(srch_n, (xmlChar *) "mode", (xmlChar *) "list");
        xmlNewChild(srch_n, NULL, (xmlChar *) "sortkeys", (xmlChar *) sortkeys);

        srch_n = xmlNewChild(srch_n, NULL, (xmlChar *) "fieldMapping", NULL);
        xmlNewProp(srch_n, (xmlChar *) "table", (xmlChar *) "certificates");

        // Look up the certificate info and display it
        certlist = eDBadminCertificate(ctx, srch_xml);
        if( certlist == NULL ) {
                xmlFreeDoc(srch_xml);
                fprintf(stderr, "%s: Failed to query for certificates\n", MODULE);
                return 1;
        }

        cert_n = eurephiaXML_getRoot(ctx, certlist, "certificates", 1);
        if( (cert_n == NULL) || atoi_nullsafe(xmlGetAttrValue(cert_n->properties, "certificates")) == 0 ) {
                printf("%s: No certificates found\n", MODULE);
                rc = 0;
                goto exit;
        }

        // Show certificate(s)
        xslt_print_xmldoc(stdout, cfg, certlist, "certificates.xsl", xsltparams);
        rc = 0;
 exit:
        xmlFreeDoc(certlist);
        xmlFreeDoc(srch_xml);
        return rc;
}


/**
 * Main function of the certs command.
 *
 * @param ctx   eurephiaCTX
 * @param sess  eurephiaSESSION of the current logged in user
 * @param cfg   eurephiaVALUES struct of the current configuration
 * @param argc  argument count for the eurephiadm command
 * @param argv  argument table for the eurephiadm command
 *
 * @return returns 0 on success, otherwise 1.
 */
int cmd_Certificates(eurephiaCTX *ctx, eurephiaSESSION *sess, eurephiaVALUES *cfg, int argc, char **argv) {
        char **mode_argv;
        int i, mode_argc = 0, rc = 0;
        int (*mode_fnc) (eurephiaCTX *ctx, eurephiaSESSION *sess, eurephiaVALUES *cfg, int argc, char **argv);

        e_options modeargs[] = {
                {"--list", "-l", 0},
                {"--add", "-A", 0},
                {"--delete", "-D", 0},
                {"--help", "-h", 0},
                {NULL, NULL, 0}
        };

        assert((ctx != NULL) && (ctx->dbc != NULL) && (ctx->dbc->config != NULL));
        mode_fnc = NULL;
        for( i = 1; i < argc; i++ ) {
                switch( eurephia_getopt(&i, argc, argv, modeargs) ) {
                case 'l':
                        mode_fnc = list_certs;
                        break;

                case 'h':
                        mode_fnc = help_Certificates2;
                        break;

                case 'A':
                        mode_fnc = add_cert;
                        break;

                case 'D':
                        mode_fnc = delete_cert;
                        break;

                default:
                        break;
                }
                if( mode_fnc != NULL ) {
                        break;
                }
        }

        // If we do not have any known mode defined, exit with error
        if( mode_fnc == NULL )  {
                fprintf(stderr, "%s: Unknown argument.  No mode given\n", MODULE);
                return 1;
        }

        // Allocate memory for our arguments being sent to the mode function
        mode_argv = (char **) calloc(sizeof(char *), (argc - i)+2);
        assert(mode_argv != NULL);

        // Copy over only the arguments needed for the mode
        mode_argc = eurephia_arraycp(i, argc, argv, mode_argv, (argc - i));

        // Call the mode function
        rc = mode_fnc(ctx, sess, cfg, mode_argc, mode_argv);
        free_nullsafe(ctx, mode_argv);

        return rc;
}
