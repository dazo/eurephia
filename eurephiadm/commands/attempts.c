/* attempts.c  --  eurephiadm attempts command:
 *                 Show/edit records registered in the attempts table
 *
 *  GPLv2 only - Copyright (C) 2009 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   eurephiadm/commands/attempts.c
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2009-05-06
 *
 * @brief  eurephiadm attempts command.  Show/edit records in the attempts table.
 *
 */
#include <stdio.h>
#include <string.h>
#include <assert.h>

#ifdef HAVE_LIBXML2
#include <libxml/tree.h>
#endif

#define MODULE "eurephia::Attempts" /**< Need to define the active module before including argparser.h */
#include <eurephia_nullsafe.h>
#include <eurephia_context.h>
#include <eurephia_log.h>
#include <eurephia_xml.h>
#include <eurephia_values_struct.h>
#include <eurephiadb_session_struct.h>
#include <eurephiadb_mapping.h>
#include <eurephiadb_driver.h>
#include <eurephia_values.h>

#include "../argparser.h"
#include "../xsltparser.h"

/**
 * Help screens for the attempts command
 *
 * @param page which help screen to display
 */
void display_attempts_help(int page)
{
        switch( page ) {
        case 'l':
                printf("The attempts list mode will show all registered login attempts.\n"
                       "\n"
                       "     -v | --verbose               Show more details\n"
                       "\n"
                       "Filters:\n"
                       "     -u | --username <username>   User name\n"
                       "     -d | --digest <SHA1 digest>  Certificate SHA1 digest\n"
                       "     -i | --ipaddr <IP address>   IP address\n\n");
                break;

        case 'D':
                printf("The attempts delete mode will remove a record from the attempts log.\n"
                       "\n"
                       "One of the following parameters must be given (only one):\n"
                       "     -u | --username <username>   User name\n"
                       "     -d | --digest <SHA1 digest>  Certificate SHA1 digest\n"
                       "     -i | --ipaddr <IP address>   IP address\n"
                       "     -a | --attemptid <ID>        Attempts record ID\n"
                       "\n"
                       );
                break;

        case 'R':
                printf("The attempts reset mode will reset the attempt registration.\n"
                       "\n"
                       "One of the following parameters must be given (only one):\n"
                       "     -u | --username <username>   User name\n"
                       "     -d | --digest <SHA1 digest>  Certificate SHA1 digest\n"
                       "     -i | --ipaddr <IP address>   IP address\n"
                       "     -a | --attemptid <ID>        Attempts record ID\n"
                       "\n"
                       );
                break;

        default:
                printf("Available modes for the attempts command are:\n\n"
                       "     -D | --delete       Delete a registered login attempt\n"
                       "     -R | --reset        Reset a registered login attempt\n"
                       "     -l | --list         List all registered login attempts\n"
                       "     -h | --help <mode>  Show help\n\n");
                break;
        }
}


/**
 * Help screen wrapper.  Used by cmd_Help()
 */
void help_Attempts()
{
        display_attempts_help(0);
}


/**
 * Help screen wrapper for the attempts help function.
 *
 * @param ctx   eurephiaCTX
 * @param sess  eurephiaSESSION of the current logged in user
 * @param cfg   eurephiaVALUES struct of the current configuration
 * @param argc  argument count for the eurephiadm command
 * @param argv  argument table for the eurephiadm command
 *
 * @return returns 0 on success, otherwise 1.
 */
int help_Attempts2(eurephiaCTX *ctx, eurephiaSESSION *sess, eurephiaVALUES *cfg, int argc, char **argv)
{
        e_options modeargs[] = {
                {"--list", "-l", 0},
                {"--reset", "-R", 0},
                {"--delete", "-D", 0},
                {NULL, NULL, 0}
        };
        int i = 1;
        display_attempts_help(eurephia_getopt(&i, argc, argv, modeargs));
        return 0;
}


/**
 * attempts list mode.  List registered failed attempts
 *
 * @param ctx   eurephiaCTX
 * @param sess  eurephiaSESSION of the current logged in user
 * @param cfg   eurephiaVALUES struct of the current configuration
 * @param argc  argument count for the eurephiadm command
 * @param argv  argument table for the eurephiadm command
 *
 * @return returns 0 on success, otherwise 1.
 */
int list_attempts(eurephiaCTX *ctx, eurephiaSESSION *sess, eurephiaVALUES *cfg, int argc, char **argv)
{
        xmlDoc *log_xml = NULL, *srch_xml = NULL;
        xmlNode *fmap_n = NULL, *srch_n = NULL;
        char *xsltparams[] = {"view", "'list'", NULL};
        int i = 0;

        e_options modeargs[] = {
                {"--verbose", "-v", 0},
                {"--help", "-h", 0},
                {"--username", "-u", 1},
                {"--digest", "-d", 1},
                {"--ipaddr", "-i", 1},
                {NULL, NULL, 0}
        };

        eurephiaXML_CreateDoc(ctx, 1, "attemptslog", &srch_xml, &srch_n);
        xmlNewProp(srch_n, (xmlChar *) "mode", (xmlChar *) "list");

        fmap_n = xmlNewChild(srch_n, NULL, (xmlChar *) "fieldMapping", NULL);
        xmlNewProp(fmap_n, (xmlChar *) "table", (xmlChar *) "attemptslog");

        for( i = 1; i < argc; i++ ) {
                switch( eurephia_getopt(&i, argc, argv, modeargs) ) {
                case 'v':
                        xsltparams[1] = "'details'";
                        break;

                case 'u':
                        xmlNewChild(fmap_n, NULL, (xmlChar *) "username", (xmlChar *) optargs[0]);
                        break;

                case 'd':
                        xmlNewChild(fmap_n, NULL, (xmlChar *) "digest", (xmlChar *) optargs[0]);
                        break;

                case 'i':
                        xmlNewChild(fmap_n, NULL, (xmlChar *) "ip", (xmlChar *) optargs[0]);
                        break;

                case 'h':
                        display_attempts_help('l');
                        xmlFreeDoc(srch_xml);
                        return 0;

                default:
                        xmlFreeDoc(srch_xml);
                        return 1;
                }
        }

        log_xml = eDBadminAttemptsLog(ctx, srch_xml);
        xmlFreeDoc(srch_xml);
        if( log_xml == NULL ) {
                fprintf(stderr, "%s: Error retrieving login attempts log.\n", MODULE);
                return 1;
        }

        xslt_print_xmldoc(stdout, cfg, log_xml, "attempts.xsl", (const char **) xsltparams);
        xmlFreeDoc(log_xml);
        return 0;
}


/**
 * Function for modifying attempts records in the database.
 *
 * @param ctx   eurephiaCTX
 * @param sess  eurephiaSESSION of the current logged in user
 * @param cfg   eurephiaVALUES struct of the current configuration
 * @param argc  argument count for the eurephiadm command
 * @param argv  argument table for the eurephiadm command
 *
 * @return returns 0 on success, otherwise 1.
 */
int modify_attempts(eurephiaCTX *ctx, eurephiaSESSION *sess, eurephiaVALUES *cfg, int argc, char **argv)
{
        xmlDoc *result_xml = NULL, *upd_xml = NULL;
        xmlNode *fmap_n = NULL, *upd_n = NULL;
        eurephiaRESULT *res = NULL;
        int i = 0, rc = 1, mode = 0;

        e_options modeargs[] = {
                {"--help", "-h", 0},
                {"--username", "-u", 1},
                {"--digest", "-d", 1},
                {"--ipaddr", "-i", 1},
                {"--attemptid", "-a", 1},
                {NULL, NULL, 0}
        };

        eurephiaXML_CreateDoc(ctx, 1, "attemptslog", &upd_xml, &upd_n);
        if( (strcmp(argv[0], "--reset") == 0) || (strcmp(argv[0], "-R") == 0) ) {
                xmlNewProp(upd_n, (xmlChar *) "mode", (xmlChar *) "reset");
                mode = 'R';
        } else if( (strcmp(argv[0], "--delete") == 0) || (strcmp(argv[0], "-D") == 0) ) {
                xmlNewProp(upd_n, (xmlChar *) "mode", (xmlChar *) "delete");
                mode = 'D';
        } else {
                fprintf(stderr, "%s: Invalid mode\n", MODULE);
                xmlFreeDoc(upd_xml);
                return 1;
        }

        fmap_n = xmlNewChild(upd_n, NULL, (xmlChar *) "fieldMapping", NULL);
        xmlNewProp(fmap_n, (xmlChar *) "table", (xmlChar *) "attemptslog");

        for( i = 1; i < argc; i++ ) {
                switch( eurephia_getopt(&i, argc, argv, modeargs) ) {
                case 'u':
                        xmlNewChild(fmap_n, NULL, (xmlChar *) "username", (xmlChar *) optargs[0]);
                        break;

                case 'd':
                        xmlNewChild(fmap_n, NULL, (xmlChar *) "digest", (xmlChar *) optargs[0]);
                        break;

                case 'i':
                        xmlNewChild(fmap_n, NULL, (xmlChar *) "ip", (xmlChar *) optargs[0]);
                        break;

                case 'a':
                        xmlNewChild(fmap_n, NULL, (xmlChar *) "id", (xmlChar *) optargs[0]);
                        break;

                case 'h':
                        display_attempts_help(mode);
                        xmlFreeDoc(upd_xml);
                        return 0;

                default:
                        xmlFreeDoc(upd_xml);
                        return 1;
                }
        }

        result_xml = eDBadminAttemptsLog(ctx, upd_xml);
        xmlFreeDoc(upd_xml);
        if( result_xml == NULL ) {
                fprintf(stderr, "%s: Error during modifying attempts register\n", MODULE);
                return 1;
        }

        res = eurephiaXML_ParseResultMsg(ctx, result_xml);
        if( res == NULL ) {
                fprintf(stderr, "%s: Error during modifying attempts register. No results returned.\n", MODULE);
                return 1;

        }

        if( res->resultType == exmlERROR ) {
                fprintf(stderr, "%s: %s\n", MODULE, res->message);
                rc = 1;
        } else {
                fprintf(stdout, "%s: %s\n", MODULE, res->message);
                rc = 0;
        }
        free_nullsafe(ctx, res);
        xmlFreeDoc(result_xml);

        return rc;
}


/**
 * Main function for the attempts command
 *
 * @param ctx   eurephiaCTX
 * @param sess  eurephiaSESSION of the current logged in user
 * @param cfg   eurephiaVALUES struct of the current configuration
 * @param argc  argument count for the eurephiadm command
 * @param argv  argument table for the eurephiadm command
 *
 * @return returns 0 on success, otherwise 1.
 */
int cmd_Attempts(eurephiaCTX *ctx, eurephiaSESSION *sess, eurephiaVALUES *cfg, int argc, char **argv)
{
        char **mode_argv;
        int rc = 0, i = 0, mode_argc = 0;
        e_options cmdargs[] = {
                {"--list", "-l", 0},
                {"--reset", "-R", 0},
                {"--delete", "-D", 0},
                {"--help", "-h", 0},
                {NULL, NULL, 0}
        };
        int (*mode_fnc) (eurephiaCTX *ctx, eurephiaSESSION *sess, eurephiaVALUES *cfg, int argc, char **argv);

        assert((ctx != NULL) && (ctx->dbc != NULL));

        mode_fnc = NULL;
        for( i = 1; i < argc; i++ ) {
                switch( eurephia_getopt(&i, argc, argv, cmdargs) ) {
                case 'l':
                        mode_fnc = list_attempts;
                        break;

                case 'R':
                case 'D':
                        mode_fnc = modify_attempts;
                        break;

                case 'h':
                        mode_fnc = help_Attempts2;

                default:
                        break;
                }
                if( mode_fnc != NULL ) {
                        break;
                }
        }

        // If we do not have any known mode defined, exit with error
        if( mode_fnc == NULL )  {
                fprintf(stderr, "%s: Unknown argument.  No mode given\n", MODULE);
                return 1;
        }

        // Allocate memory for our arguments being sent to the mode function
        mode_argv = (char **) calloc(sizeof(char *), (argc - i)+2);
        assert(mode_argv != NULL);

        // Copy over only the arguments needed for the mode
        mode_argc = eurephia_arraycp(i, argc, argv, mode_argv, (argc - i));

        // Call the mode function
        rc = mode_fnc(ctx, sess, cfg, mode_argc, mode_argv);
        free_nullsafe(ctx, mode_argv);

        return rc;

}
