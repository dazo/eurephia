/* plugins.c  --  eurephiadm plugins command:
 *                   Manages eurephia plug-ins
 *
 *  GPLv2 only - Copyright (C) 2013
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   plugins.c
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2013-03-04
 *
 * @brief  eurephia plugins command, to manage eurephia plug-ins.
 *
 */

#include <stdio.h>
#include <string.h>
#include <assert.h>

#ifdef HAVE_LIBXML2
#include <libxml/tree.h>
#endif

#define MODULE "eurephia::Plugins" /**< Needed to define the active module before including argparser.h */
#include <eurephia_nullsafe.h>
#include <eurephia_context.h>
#include <eurephia_log.h>
#include <eurephia_xml.h>
#include <eurephia_values_struct.h>
#include <eurephiadb_session_struct.h>
#include <eurephiadb_mapping.h>
#include <eurephiadb_driver.h>
#include <eurephia_values.h>
#include <eurephia_authplugin_driver.h>
#include <eurephia_authplugin_func.h>

#include "../argparser.h"
#include "../xsltparser.h"


/**
 * Help screens for the plugins command
 *
 * @param page which help screen to display
 */
void display_plugins_help(int page)
{
        switch( page ) {
        case 'l':
                printf("The plugins list mode will show all registered eurephia plug-ins.\n"
                       "\n"
                       "     -v | --verbose               Print more plug-in info\n"
                       "\n"
                       "Filters:\n"
                       "     -t | --plugin-type <id>      Plug-in type ID string\n"
                       );
                break;

        case 'R':
                printf("The plugins register mode will register a new eurephia plug-in.\n"
                       "\n"
                       "     -p | --plugin <file>   Full path to the plug-in to register\n"
                       "     -c | --config <string> Set the configuration string for this plug-in"
                       "\n"
                       );
                break;

        case 'U':
                printf("The plugins unregister mode will remove a registered plug-in.\n"
                       "\n"
                       "     -p | --plugin <file>   Full path to the plug-in to register\n"
                       "     -i | --plugin-id <ID>  Nummeric ID to the plug-in\n"
                       "\n"
                       );
                break;

        case 'M':
                printf("The plugins modify mode will update the configuration string.\n"
                       "\n"
                       "     -p | --plugin <file>   Full path to the plug-in to register\n"
                       "     -i | --plugin-id <ID>  Nummeric ID to the plug-in\n"
                       "     -c | --config <string> Set the configuration string for this plug-in\n"
                       "     -d | --disable         Disables the plug-in\n"
                       "     -e | --enable          Enables the plug-in\n"
                       "\n"
                       );
                break;

        default:
                printf("Available modes for the plugins command are:\n\n"
                       "     -R | --register     Register a new plug-in\n"
                       "     -U | --unregister   Remove a registered plug-in\n"
                       "     -M | --modify       Modify plug-in parameters\n"
                       "     -l | --list         List registered plug-ins\n"
                       "     -h | --help <mode>  Show help\n\n");
                break;
        }
}


/**
 * Help screen wrapper.  Used by cmd_Help()
 */
void help_plugins()
{
        display_plugins_help(0);
}


/**
 * Help screen wrapper for the plugins help function.
 *
 * @param ctx   eurephiaCTX
 * @param sess  eurephiaSESSION of the current logged in user
 * @param cfg   eurephiaVALUES struct of the current configuration
 * @param argc  argument count for the eurephiadm command
 * @param argv  argument table for the eurephiadm command
 *
 * @return returns 0 on success, otherwise 1.
 */
int help_plugins2(eurephiaCTX *ctx, eurephiaSESSION *sess, eurephiaVALUES *cfg, int argc, char **argv)
{
        e_options pluginsargs[] = {
                {"--list", "-l", 0},
                {"--register", "-R", 0},
                {"--unregister", "-U", 0},
                {"--modify", "-M", 0},
                {NULL, NULL, 0}
        };
        int i = 1;
        display_plugins_help(eurephia_getopt(&i, argc, argv, pluginsargs));
        return 0;
}


/**
 * plugins list mode.  Lists all registered eurephia plugins.
 *
 * @param ctx   eurephiaCTX
 * @param sess  eurephiaSESSION of the current logged in user
 * @param cfg   eurephiaVALUES struct of the current configuration
 * @param argc  argument count for the eurephiadm command
 * @param argv  argument table for the eurephiadm command
 *
 * @return returns 0 on success, otherwise 1.
 */
static int list_plugins(eurephiaCTX *ctx, eurephiaSESSION *sess, eurephiaVALUES *cfg, int argc, char **argv)
{
        xmlDoc *plugins_xml = NULL, *srch_xml = NULL;
        xmlNode *fmap_n = NULL, *srch_n = NULL;
        char *xsltparams[] = {"view", "'list'", NULL};
        int i = 0;

        e_options pluginsargs[] = {
                {"--help", "-h", 0},
                {"--plugin-type", "-t", 1},
                {"--verbose", "-v", 0},
                {NULL, NULL, 0}
        };

        eurephiaXML_CreateDoc(ctx, 1, "plugins", &srch_xml, &srch_n);
        xmlNewProp(srch_n, (xmlChar *) "mode", (xmlChar *) "search");

        fmap_n = xmlNewChild(srch_n, NULL, (xmlChar *) "fieldMapping", NULL);
        xmlNewProp(fmap_n, (xmlChar *) "table", (xmlChar *) "plugins");

        for( i = 1; i < argc; i++ ) {
                switch( eurephia_getopt(&i, argc, argv, pluginsargs) ) {
                case 'v':
                        xsltparams[1] = "'details'";
                        break;

                case 't':
                        xmlNewChild(fmap_n, NULL,
                                    (xmlChar *) "type", (xmlChar *) optargs[0]);
                        break;

                case 'h':
                        display_plugins_help('l');
                        xmlFreeDoc(srch_xml);
                        return 0;

                default:
                        xmlFreeDoc(srch_xml);
                        return 1;
                }
        }

        plugins_xml = eDBadminPlugins(ctx, srch_xml);
        xmlFreeDoc(srch_xml);
        if( plugins_xml == NULL ) {
                fprintf(stderr, "%s: Error retrieving information about eurephia plug-ins\n",
                        MODULE);
                return 1;
        }
        xslt_print_xmldoc(stdout, cfg, plugins_xml, "plugins.xsl", (const char **) xsltparams);
        xmlFreeDoc(plugins_xml);

        return 0;
}


/**
 * plugins register/unregister/modify mode.  Registers, unregisters plug-ins
 * or modify plug-in settings
 *
 * @param ctx   eurephiaCTX
 * @param sess  eurephiaSESSION of the current logged in user
 * @param cfg   eurephiaVALUES struct of the current configuration
 * @param argc  argument count for the eurephiadm command
 * @param argv  argument table for the eurephiadm command
 *
 * @return returns 0 on success, otherwise 1.
 */
static int plugins_process(eurephiaCTX *ctx, eurephiaSESSION *sess, eurephiaVALUES *cfg, int argc, char **argv)
{
        xmlDoc *result_xml = NULL, *srch_xml = NULL;
        xmlNode *fmap_n = NULL, *srch_n = NULL;
        eurephiaRESULT *res = NULL;
        int i = 0, rc = 1, mode = 0;
        ePluginInfoRW *pluginf = NULL;
        int enabled = 0, disabled = 0;

        e_options registerargs[] = {
                        {"--help", "-h", 0},
                        {"--plugin", "-p", 1},
                        {"--config", "-c", 1},
                        {NULL, NULL, 0}
        };

        e_options unregisterargs[] = {
                        {"--help", "-h", 0},
                        {"--plugin", "-p", 1},
                        {"--plugin-id", "-i", 1},
                        {NULL, NULL, 0}
        };

        e_options modifyargs[] = {
                        {"--help", "-h", 0},
                        {"--plugin", "-p", 1},
                        {"--plugin-id", "-i", 1},
                        {"--config", "-c", 1},
                        {"--disable", "-d", 0},
                        {"--enable", "-e", 0},
                        {NULL, NULL, 0}
        };

        e_options *pluginsargs = NULL;

        eurephiaXML_CreateDoc(ctx, 1, "plugins", &srch_xml, &srch_n);
        if( (strcmp(argv[0], "--register") == 0) || (strcmp(argv[0], "-R") == 0) ) {
                xmlNewProp(srch_n, (xmlChar *) "mode", (xmlChar *) "register");
                pluginsargs = registerargs;
                mode = 'R';
        } else if( (strcmp(argv[0], "--unregister") == 0) || (strcmp(argv[0], "-U") == 0) ) {
                xmlNewProp(srch_n, (xmlChar *) "mode", (xmlChar *) "unregister");
                pluginsargs = unregisterargs;
                mode = 'U';
        } else if( (strcmp(argv[0], "--modify") == 0) || (strcmp(argv[0], "-M") == 0) ) {
                xmlNewProp(srch_n, (xmlChar *) "mode", (xmlChar *) "modify");
                pluginsargs = modifyargs;
                mode = 'M';
        } else {
                fprintf(stderr, "%s: Invalid mode\n", MODULE);
                xmlFreeDoc(srch_xml);
                return 1;
        }

        fmap_n = xmlNewChild(srch_n, NULL, (xmlChar *) "fieldMapping", NULL);
        xmlNewProp(fmap_n, (xmlChar *) "table", (xmlChar *) "plugins");

        for( i = 1; i < argc; i++ ) {
                switch( eurephia_getopt(&i, argc, argv, pluginsargs) ) {
                case 'p':
                        if( mode == 'R' ) {
                                pluginf = ePluginInspect(ctx, optargs[0]);
                                if( pluginf == NULL ) {
                                        fprintf(stderr,
                                                "Failed to extract plug-in info from '%s'\n",
                                                optargs[0]);
                                        xmlFreeDoc(srch_xml);
                                        return 1;
                                }
                                fprintf(stdout, "Plugin file: %s\n", optargs[0]);
                                fprintf(stdout, "Plugin name: %s (v%s)\n",
                                        pluginf->name, pluginf->version);
                                fprintf(stdout, "Plugin type: %s\n",
                                        ePluginTypeString((ePluginInfo *) pluginf));
                                xmlNewChild(fmap_n, NULL, (xmlChar *) "name",
                                            (xmlChar *) pluginf->name);
                                xmlNewChild(fmap_n, NULL, (xmlChar *) "type",
                                            (xmlChar *) ePluginTypeShortString((ePluginInfo *) pluginf));
                                xmlNewChild(fmap_n, NULL, (xmlChar *) "dsofile",
                                            (xmlChar *) optargs[0]);
                                ePluginInfoFree(ctx, pluginf);
                        } else if( mode == 'U' ) {
                                xmlNewChild(fmap_n, NULL, (xmlChar *) "dsofile",
                                            (xmlChar *) optargs[0]);
                        } else if( mode == 'M' ) {
                                xmlNewProp(srch_n, (xmlChar *) "plugin-dso",
                                           (xmlChar *) optargs[0]);
                        }
                        break;

                case 'i':
                        if( mode == 'U' ) {
                                xmlNewChild(fmap_n, NULL, (xmlChar *) "plugin_id",
                                            (xmlChar *) optargs[0]);
                        } else if ( mode == 'M' ) {
                                xmlNewProp(srch_n, (xmlChar *) "plugin-id",
                                           (xmlChar *) optargs[0]);
                        }
                        break;

                case 'c':
                        xmlNewChild(fmap_n, NULL, (xmlChar *) "config", (xmlChar *) optargs[0]);
                        break;

                case 'd':
                        if( !enabled ) {
                                disabled = 1;
                                xmlNewChild(fmap_n, NULL, (xmlChar *) "enabled", (xmlChar *) "f");
                        } else {
                                fprintf(stderr, "%s: --disabled and --enabled are mutually exclusive\n", MODULE);
                                return 1;
                        }
                        break;

                case 'e':
                        if( !disabled ) {
                                enabled = 1;
                                xmlNewChild(fmap_n, NULL, (xmlChar *) "enabled", (xmlChar *) "t");
                        } else {
                                fprintf(stderr, "%s: --enable and --disabled are mutually exclusive\n", MODULE);
                                return 1;
                        }
                        break;

                case 'h':
                        display_plugins_help(mode);
                        return 0;

                default:
                        return 1;
                }
        }

        result_xml = eDBadminPlugins(ctx, srch_xml);
        xmlFreeDoc(srch_xml);
        if( result_xml == NULL ) {
                fprintf(stderr, "%s: Error while processing eurephia plug-in request\n", MODULE);
                return 1;
        }

        res = eurephiaXML_ParseResultMsg(ctx, result_xml);
        if( res == NULL ) {
                fprintf(stderr, "%s: Error while processing plug-in request. No results returned\n", MODULE);
                return 1;

        } else {
                if( res->resultType == exmlERROR ) {
                        fprintf(stderr, "%s: %s\n", MODULE, res->message);
                        rc = 1;
                } else {
                        fprintf(stdout, "%s: %s\n", MODULE, res->message);
                        rc = 0;
                }
        }
        free_nullsafe(ctx, res);
        xmlFreeDoc(result_xml);

        return rc;
}


/**
 * Main function for the plugins command
 *
 * @param ctx   eurephiaCTX
 * @param sess  eurephiaSESSION of the current logged in user
 * @param cfg   eurephiaVALUES struct of the current configuration
 * @param argc  argument count for the eurephiadm command
 * @param argv  argument table for the eurephiadm command
 *
 * @return returns 0 on success, otherwise 1.
 */
int cmd_plugins(eurephiaCTX *ctx, eurephiaSESSION *sess, eurephiaVALUES *cfg, int argc, char **argv)
{
        char **mode_argv;
        int rc = 0, i = 0, mode_argc = 0;
        e_options pluginsargs[] = {
                {"--list", "-l", 0},
                {"--register", "-R", 0},
                {"--unregister", "-U", 0},
                {"--modify", "-M", 0},
                {"--help", "-h", 0},
                {NULL, NULL, 0}
        };
        int (*mode_fnc) (eurephiaCTX *ctx, eurephiaSESSION *sess, eurephiaVALUES *cfg, int argc, char **argv);

        assert((ctx != NULL) && (ctx->dbc != NULL));

        mode_fnc = NULL;
        for( i = 1; i < argc; i++ ) {
                switch( eurephia_getopt(&i, argc, argv, pluginsargs) ) {
                case 'l':
                        mode_fnc = list_plugins;
                        break;

                case 'R':
                case 'U':
                case 'M':
                        mode_fnc = plugins_process;
                        break;

                case 'h':
                        mode_fnc = help_plugins2;

                default:
                        break;
                }
                if( mode_fnc != NULL ) {
                        break;
                }
        }

        // If we do not have any known mode defined, exit with error
        if( mode_fnc == NULL )  {
                fprintf(stderr, "%s: Unknown argument.  No mode given\n", MODULE);
                return 1;
        }

        // Allocate memory for our arguments being sent to the mode function
        mode_argv = (char **) calloc(sizeof(char *), (argc - i)+2);
        assert(mode_argv != NULL);

        // Copy over only the arguments needed for the mode
        mode_argc = eurephia_arraycp(i, argc, argv, mode_argv, argc);

        // Call the mode function
        rc = mode_fnc(ctx, sess, cfg, mode_argc, mode_argv);
        free_nullsafe(ctx, mode_argv);

        return rc;

}
