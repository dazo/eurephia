/* buildinfo.c   --  eurephiadm buildinfo command:
 *                   Shows info about the eurephia build
 *
 *  GPLv2 only - Copyright (C) 2009 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   eurephiadm/commands/buildinfo.c
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2010-11-09
 *
 * @brief  eurephiadm buildinfo command.  Debug info related to the eurephia build
 *
 */

#include <stdio.h>
#include <string.h>
#include <assert.h>

#ifdef HAVE_LIBXML2
#include <libxml/tree.h>
#include <libxml/xpath.h>
#endif

#define MODULE "eurephia::BuildInfo" /**< Need to define the active module before including argparser.h */
#include <eurephia_nullsafe.h>
#include <eurephia_context.h>
#include <eurephia_log.h>
#include <eurephia_values_struct.h>
#include <eurephiadb_session_struct.h>

#include "../argparser.h"
#include "../buildinfo.h"

/**
 *  Help screen for the buildinfo command
 */
void help_BuildInfo()
{
        printf("eurephiadm::BuildInfo\n\n"
               "This command will just show some basic information about the\n"
               "current eurephia build.  This is mostly useful when reporting bugs.\n\n"
               );
}


/**
 *  Main function for the buildinfo command.  Does the querying of the database and show the result
 *
 * @param ctx   eurephiaCTX
 * @param sess  eurephiaSESSION of the current logged in user
 * @param cfg   eurephiaVALUES struct of the current configuration
 * @param argc  argument count for the eurephiadm command
 * @param argv  argument table for the eurephiadm command
 *
 * @return returns 0 on success, otherwise 1.
 */
int cmd_BuildInfo(eurephiaCTX *ctx, eurephiaSESSION *sess, eurephiaVALUES *cfg, int argc, char **argv)
{
        printf("**\n** eurephia build information\n**\n\n"
               " * Generic information *\n"
               "   Version:        " EUREPHIAVERSION "\n"
               "   Source base:    " BUILDSOURCE "\n"
               "   Build date:     " BUILDDATE "\n"
               "   Build host:     " BUILDHOST  "\n"
               "   System info:    " BUILDSYSTEM  "\n"
               "   Configure:      " BUILDCONFIGURE "\n");

#ifdef BUILDBRANCH
        printf("\n * git information *\n");
        printf("   repo dir:       " GITDIR "\n"
               "   branch:         " BUILDBRANCH "\n"
               "   HEAD ref:       " BUILDGITREV  "\n");
        printf("   tree state:     %s\n",
               (MODIFIEDCOUNT == 0 ? "Clean" : "Modified"));
        if( MODIFIEDCOUNT != 0 ) {
                printf("\n   Uncommitted changes to the git tree (%i file%s):\n"
                       MODIFIEDFILES, MODIFIEDCOUNT, (MODIFIEDCOUNT != 1 ? "s" : ""));
        }
#endif
        printf("\n");
        return 0;
}
