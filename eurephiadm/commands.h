/* commands.h  --  All eurephiadm commands are declared here
 *                 The function is found in the ./commands dir
 *                 where each command have their own file
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   commands.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2008-12-01
 *
 * @brief  All eurephiadm commands must be declared here.  It defines the overview over
 *         valid commands and the function pointers for the command itself plus its help
 *         function.
 *
 */

#ifndef   	EUREPHIADM_COMMANDS_H_
# define   	EUREPHIADM_COMMANDS_H_

/**
 * struct used for definition of commands, help info and function pointers to the command
 */
typedef struct {
        char *command;          /**< Command name, which is used by the end user */
        int need_session;       /**< Set this flag to 1, if the user must log in to use this command */
        char *accesslvl;        /**< String containing the access level the user need to use this command  */
        char *arghint;          /**< Simple help hints string, used in the main help screen mainly */
        char *helpinfo;         /**< Simple one-line help string describing the command */
        void (*helpfunc)(void); /**< Function pointer to the help function. Takes no arguments */
        /**
         * Commands main entry function.  This function will be called when a valid function is found
         *
         * @param eurephiaCTX* The current eurephiaCTX will be received here.
         * @param eurephiaSESSION* The current eurephiaSESSION for the logged in user.
         * @param argc argument counter of all following arguments.
         * @param argv argument vector, char * array of all arguments. arg[0] is the command itself.
         * @return The function must return 0 on success, and a value > 0 on errors.
         */
        int (*function)(eurephiaCTX *, eurephiaSESSION *, eurephiaVALUES *, int argc, char **argv);
} eurephiadm_functions;

/**< eurephiadm commands, found in eurephiadm.c  */
void help_Help();
int cmd_Help(eurephiaCTX *, eurephiaSESSION *, eurephiaVALUES *cfg, int argc, char **argv);
int cmd_Logout(eurephiaCTX *, eurephiaSESSION *, eurephiaVALUES *cfg, int argc, char **argv);
int cmd_ShowCfg(eurephiaCTX *, eurephiaSESSION *, eurephiaVALUES *cfg, int argc, char **argv);


/*  Commands and help functions listed here are found in ./commands/{command}.c files - one file per command*/
void help_Users();
int cmd_Users(eurephiaCTX *, eurephiaSESSION *, eurephiaVALUES *cfg, int argc, char **argv);

void help_Lastlog();
int cmd_Lastlog(eurephiaCTX *, eurephiaSESSION *, eurephiaVALUES *cfg, int argc, char **argv);

void help_Attempts();
int cmd_Attempts(eurephiaCTX *, eurephiaSESSION *, eurephiaVALUES *cfg, int argc, char **argv);

void help_Blacklist();
int cmd_Blacklist(eurephiaCTX *, eurephiaSESSION *, eurephiaVALUES *cfg, int argc, char **argv);

void help_Certificates();
int cmd_Certificates(eurephiaCTX *, eurephiaSESSION *, eurephiaVALUES *cfg, int argc, char **argv);

void help_UserCerts();
int cmd_UserCerts(eurephiaCTX *, eurephiaSESSION *, eurephiaVALUES *cfg, int argc, char **argv);

void help_plugins();
int cmd_plugins(eurephiaCTX *, eurephiaSESSION *, eurephiaVALUES *cfg, int argc, char **argv);

void help_fwProfiles();
int cmd_fwProfiles(eurephiaCTX *, eurephiaSESSION *, eurephiaVALUES *cfg, int argc, char **argv);

void help_AdminAccess();
int cmd_AdminAccess(eurephiaCTX *, eurephiaSESSION *, eurephiaVALUES *cfg, int argc, char **argv);

void help_EditConfig();
int cmd_EditConfig(eurephiaCTX *, eurephiaSESSION *, eurephiaVALUES *cfg, int argc, char **argv);

void help_BuildInfo();
int cmd_BuildInfo(eurephiaCTX *, eurephiaSESSION *, eurephiaVALUES *cfg, int argc, char **argv);

/**
 * Declaration of all valid commands
 * {command,  need_session,  acclvl,  arghints,
 *  helpinfo,  helpdescr,  function}
 */
static const eurephiadm_functions cmdline_functions[] = {

        {"help",            0, NULL,        "[<command>]",
         "This help screen",                       help_Help, cmd_Help},

        {"logout",          1, NULL,        NULL,  // Logout is specially handled - change with care
         "Logout from an open session",            NULL, cmd_Logout},

        {"users",       1, "useradmin", "-l",
         "User management",                        help_Users, cmd_Users},

        {"lastlog",     1, "useradmin", NULL,
         "Query the eurephia lastlog",               help_Lastlog, cmd_Lastlog},

        {"attempts",    1, "attempts",  NULL,
         "Show/edit registered login attempts",      help_Attempts, cmd_Attempts},

        {"blacklist",   1, "blacklist",  NULL,
         "Show/edit blacklisted items",              help_Blacklist, cmd_Blacklist},

        {"certs",       1, "certadmin", NULL,
         "Certificate management",                 help_Certificates, cmd_Certificates},

        {"usercerts",   1, "useradmin", NULL,
         "User account/Certificate link management", help_UserCerts, cmd_UserCerts},

        {"plugins",   1, "config", NULL,
         "Plug-in management", help_plugins, cmd_plugins},

        {"fwprofiles",  1, "fwprofiles", NULL,
         "Firewall profile management",               help_fwProfiles,   cmd_fwProfiles},

        {"adminaccess",  1, "useradmin", NULL,
         "User account access levels (admin)",      help_AdminAccess, cmd_AdminAccess},

        {"show-config",     1, "config",    NULL,
         "List all config settings",               NULL, cmd_ShowCfg},

        {"show-configfile", 0, NULL,        NULL,
         "List only config file settings",         NULL, cmd_ShowCfg},

        {"config",          1, "config",    "[-s|-d] <key> [<val>] | [-l]",
         "Add, delete or show one config setting", help_EditConfig, cmd_EditConfig},

        {"buildinfo",       0, "buildinfo", NULL,
         "Show information related to the eurephia build", help_BuildInfo, cmd_BuildInfo},

        // End of records marker
        {NULL,              0, NULL,        NULL,
         NULL,                                     NULL, NULL}
};

#endif 	    /* !COMMANDS_H_ */
