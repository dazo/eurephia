/*  client_config.h  --  Handles reading and parsing of config files
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
*/

/**
 * @file   client_config.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2008-12-01
 *
 * @brief  Config file parser
 *
 */

#ifndef EUREPHIA_CLIENT_CONFIG_H
#define EUREPHIA_CLIENT_CONFIG_H

#include <eurephia_values.h>

char *get_config_filename(const char *env, const char *file);
eurephiaVALUES *ReadConfig(const char *env, const char *cfgname);

#endif
