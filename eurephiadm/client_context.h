/* client_context.h  --  Handles eurephia contexts used by admin interfaces
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
*/

/**
 * @file   client_context.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2008-12-01
 *
 * @brief  Functions for working with eurephiaCTX outside the openvpn plug-in
 *
 */

#ifndef EUREPHIA_CLIENT_CONTEXT
#define EUREPHIA_CLIENT_CONTEXT

eurephiaCTX *eurephiaCTX_init(const char *logident, const char *log, const int loglevel, eurephiaVALUES *cfg);
void eurephiaCTX_destroy(eurephiaCTX *ctx);

#endif
