/*  dummy-auth.c  -- Stupid dummy authentication - ONLY FOR TESTING PURPOSES
 *
 *  Copyright (C) 2013          David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * @file   dummy-auth.c
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2013-02-15
 *
 * @brief  Simple and stupid dummy authentication. ONLY FOR TESTING PURPOSES
 */

#include <stdio.h>
#include <string.h>
#include <eurephia_nullsafe.h>
#include <eurephia_context.h>
#include <eurephia_log.h>
#include <eurephia_authplugin_driver.h>

static ePluginInfo pluginfo = { .name = "Dummy-Authentication plug-in",
                                .version = "1.0",
                                .copyright = "2013 (C) David Sommerseth <dazo@users.sourceforge.net>",
                                .pluginType = eptAUTH,
                                .APIversion = 1 };


/**
 * @copydoc PluginInfo()
 */
ePluginInfo * PluginInfo()
{
        return &pluginfo;
}

/**
 * @copydoc PluginInit()
 */
int PluginInit(eurephiaCTX *ctx, const char *args)
{
        eurephia_log(ctx, LOG_INFO, 0, "Dummy auth initialised: %s", args);
        eurephia_log(ctx, LOG_WARNING, 0, "DO NOT USE dummy-auth IN PRODUCTION");
        return 1;
}


/**
 * @copydoc PluginClose()
 */
void PluginClose(eurephiaCTX *ctx)
{
        eurephia_log(ctx, LOG_INFO, 0, "Closing Dummy auth");
}


/**
 * Generic dummy authentication used by both AuthenticateUser() and
 * ChangePassword().  This particular implementation is not safe to use in
 * a production environment, as it only considers hard coded passwords
 * for testing purposes.
 *
 * @param eurephiCTX*  Pointer to the global eurephia context
 * @param username*    Char pointer to a buffer containing the username
 * @param passwd*      Char pointer to a buffer containing the password in
 *                     clear text
 *
 * @return Returns a pointer to an eAuthResult struct with the authentication
 *         result.  On generic system failures, it may return NULL.
 */
eAuthResult * _dummy_auth(eurephiaCTX *ctx, const char *username, const char *passwd)
{
        eAuthResult *res = NULL;

        res = malloc_nullsafe(ctx, sizeof(eAuthResult)+2);
        if( strcmp(passwd, "correct") == 0) {
                res->status = eAUTH_SUCCESS;
                res->msg = strdup("User authenticated successfully");
        } else if( strcmp(passwd, "wrong") == 0){
                res->status = eAUTH_FAILED;
                res->msg = strdup("User authentication forced to fail");
        } else {
                res->status = eAUTH_PLGERROR;
                res->msg = strdup("Authentication plug-in failed");
        }
        return res;
}


/**
 * @copydoc AuthenticateUser()
 */
eAuthResult * AuthenticateUser(eurephiaCTX *ctx, const char *username, const char *passwd)
{
        eurephia_log(ctx, LOG_INFO, 0, "dummy-auth:AuthenticateUser('%s', '%s')",
                     username, passwd);
        return _dummy_auth(ctx, username, passwd);
}


/**
 * @copydoc ChangePassword()
 */
eAuthResult * ChangePassword(eurephiaCTX *ctx, const char *username,
                          const char *oldpass, const char *newpass)
{
        eurephia_log(ctx, LOG_INFO, 0, "dummy-auth:ChangePassword('%s', '%s', '%s')",
                     username, oldpass, newpass);
        return _dummy_auth(ctx, username, newpass);
}

