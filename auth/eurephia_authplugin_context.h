/*  eurephia_authplugin_context.h
 *
 *  Copyright (C) 2013          David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * @file   eurephia_authplugin_context.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2013-02-15
 *
 * @brief  eurephia authentication plug-in context definition
 */

#ifndef EUREPHIA_AUTHPLUGIN_CONTEXT_H_
#define EUREPHIA_AUTHPLUGIN_CONTEXT_H_

#ifdef EUREPHIA_AUTHPLUGIN_C_
/**
 *  Authentication plug-in context.
 *  Contains an overview of all loaded authentication plug-ins
 *
 *  This struct is only visible for eurephia_authplugin.c, to
 *  avoid nesting troubles when eurephiaCTX needs declared eAuthPlugin
 *  which again needs eurephiaCTX to already be defined.
 *
 */
typedef struct __eAuthPluginCTX {
        char *filename;           /**<  Filename of the plug-in */
        void *dlhandle;           /**<  Handle to plug-in returned by dlopen() */
        unsigned int plgid;       /**<  Name of the plug-in, retrieved from the database */
        eAuthPlugin *plugin;      /**<  Access to the plug-in funcions */

        struct __eAuthPluginCTX *next;  /**<  Pointer to the next auth plug-in */
} eAuthPluginCTX;
#else
typedef struct __eAuthPluginCTX eAuthPluginCTX;
#endif /* EUREPHIA_AUTHPLUGIN_C_ */


#endif /* EUREPHIA_AUTHPLUGIN_CONTEXT_H_ */
