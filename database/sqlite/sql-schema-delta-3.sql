--
-- eurephia database schema for SQLite3
--
-- This SQL scripts updates the previous SQL schema to the
-- new schema needed by edb-sqlite v1.4
--
-- GPLv2 only - Copyright (C) 2013
--              David Sommerseth <dazo@users.sourceforge.net>
--
--  This program is free software; you can redistribute it and/or
--  modify it under the terms of the GNU General Public License
--  as published by the Free Software Foundation; version 2
--  of the License.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program; if not, write to the Free Software
--  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
--

-- eurephia_plugins - Defines loadable eurephia plug-ins
-- This table is parsed upon start-up of OpenVPN/eurephia
CREATE TABLE eurephia_plugins (
       plgname        varchar(32) NOT NULL, -- short name of the plug-in module
       plgtype        varchar(16) NOT NULL, -- plug-in type (auth, firewall, etc)
       plgdsofile     text        NOT NULL, -- full path to the plug-in DSO file
       plgconfig      text        ,         -- Optional config data for the plug-in
       plgenabled     boolean     NOT NULL, -- Enable/disable the plug-in
       plgid          integer     PRIMARY KEY AUTOINCREMENT
);
CREATE INDEX eurephia_plugins_name_type ON eurephia_plugins (plgname, plgtype);

ALTER TABLE openvpn_usercerts ADD COLUMN authplugin integer;
ALTER TABLE openvpn_usercerts ADD COLUMN authusername text;
