/* blacklist.c  --  Functions for processing openvpn_blacklist records
 *
 *  GPLv2 only - Copyright (C) 2009 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   sqlite/administration/blacklist.c
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2009-05-09
 *
 * @brief  Functions for administering the blacklist table
 *
 */

#include <string.h>
#include <unistd.h>
#include <assert.h>

#include <libxml/tree.h>

#include <sqlite3.h>

#include <eurephia_nullsafe.h>
#include <eurephia_context.h>
#include <eurephia_log.h>
#include <eurephia_xml.h>
#include <eurephia_values.h>
#include <eurephiadb_session_struct.h>
#include <eurephiadb_mapping.h>
#include <passwd.h>

#include "../sqlite.h"

#define FMAP_OVPNBLACKLIST      /**< fieldmapping.h: Include declaration of tbl_sqlite_blacklist */
#include "../fieldmapping.h"


/**
 * Internal function. Retrieves a list of all registered entries in the blacklist table
 *
 * @param ctx  eurephiaCTX
 * @param fmap eDBfieldMap containing the search query
 *
 * @return Returns a valid eurephia XML document on success, otherwise NULL
 */
xmlDoc *blacklist_list(eurephiaCTX *ctx, eDBfieldMap *fmap) {
        dbresult *res = NULL;
        xmlDoc *doc = NULL;
        xmlNode *root_n = NULL, *uname_n = NULL, *cert_n = NULL, *remip_n = NULL;
        int i = 0;

        // Query the database for registered attempts
        res = sqlite_query_mapped(ctx, SQL_SELECT,
                                  "SELECT username, lower(digest), remoteip,"
                                  "       locdt(registered), locdt(last_accessed), blid"
                                  "  FROM openvpn_blacklist",
                                  NULL, fmap, "blid");
        if( sqlite_query_status(res) != dbSUCCESS ) {
                eurephia_log(ctx, LOG_ERROR, 0, "Error querying the blacklist register");
                sqlite_log_error(ctx, res);
                sqlite_free_results(res);
                return NULL;
        }

        eurephiaXML_CreateDoc(ctx, 1, "blacklist", &doc, &root_n);
        xmlNewProp(root_n, (xmlChar *) "mode", (xmlChar *) "list");

        for( i = 0; i < sqlite_get_numtuples(res); i++ ) {
                xmlNode *blist_n = NULL;

                if( sqlite_get_value(res, i, 0) != NULL ) { // Username
                        if( uname_n == NULL ) {
                                uname_n = xmlNewChild(root_n, NULL, (xmlChar *) "username", NULL);
                                assert( uname_n != NULL );
                        }
                        blist_n = xmlNewChild(uname_n, NULL, (xmlChar *) "blacklisted", NULL);
                        sqlite_xml_value(blist_n, XML_NODE, "username", res, i, 0);
                } else if( sqlite_get_value(res, i, 1) != NULL ) { // Digest
                        if( cert_n == NULL ) {
                                cert_n  = xmlNewChild(root_n, NULL, (xmlChar *) "certificate", NULL);
                                assert( cert_n != NULL );
                        }
                        blist_n = xmlNewChild(cert_n, NULL, (xmlChar *) "blacklisted", NULL);
                        sqlite_xml_value(blist_n, XML_NODE, "certificate", res, i, 1);
                } else if( sqlite_get_value(res, i, 2) != NULL ) { // IP address
                        if( remip_n == NULL ) {
                                remip_n = xmlNewChild(root_n, NULL, (xmlChar *) "ipaddress", NULL);
                                assert( remip_n != NULL );
                        }
                        blist_n = xmlNewChild(remip_n, NULL, (xmlChar *) "blacklisted", NULL);
                        sqlite_xml_value(blist_n, XML_NODE, "ipaddress", res, i, 2);
                } else {
                        continue;
                }

                sqlite_xml_value(blist_n, XML_ATTR, "blid", res, i, 5);
                sqlite_xml_value(blist_n, XML_NODE, "registered", res, i, 3);
                sqlite_xml_value(blist_n, XML_NODE, "last_accessed", res, i, 4);
        }
        sqlite_free_results(res);
        return doc;
}


/**
 * Internal function.  Registers a new entry in the blacklist.
 *
 * @param ctx   eurephiaCTX
 * @param fmap  eDBfieldMap containing the entry to be registered
 *
 * @return Returns an eurephia ResultMsg XML document, with success message or an error message
 */
xmlDoc *blacklist_add(eurephiaCTX *ctx, eDBfieldMap *fmap) {
        dbresult *res = NULL;
        xmlDoc *ret = NULL;
        int fields = 0;

        fields = eDBmappingFieldsPresent(fmap);
        if( (fields != FIELD_UNAME) && (fields != FIELD_CERTDIGEST) && (fields != FIELD_REMOTEIP) ) {
                return eurephiaXML_ResultMsg(ctx, exmlERROR, NULL,
                                             "Missing username, IP address or certificate digest, "
                                             "or multiple of these fields were given.");
        }

        res = sqlite_query_mapped(ctx, SQL_INSERT, "INSERT INTO openvpn_blacklist", fmap, NULL, NULL);
        if( sqlite_query_status(res) == dbSUCCESS ) {
                ret = eurephiaXML_ResultMsg(ctx, exmlRESULT, NULL, "Record registered in the blacklist");
        } else {
                xmlNode *err_n = NULL;

                eurephia_log(ctx, LOG_FATAL, 0, "Could not blacklist the requested data");
                err_n = sqlite_log_error_xml(ctx, res);
                ret = eurephiaXML_ResultMsg(ctx, exmlERROR, err_n, "Blacklisting failed");
                xmlFreeNode(err_n);
        }
        sqlite_free_results(res);
        return ret;
}


/**
 * Internal function.  Deletes an entry from the blacklist
 *
 * @param ctx   eurephiaCTX
 * @param fmap  eDBfieldMap containing the entry to be deleted
 *
 * @return Returns an eurephia ResultMsg XML document, with success message or an error message
 */
xmlDoc *blacklist_delete(eurephiaCTX *ctx, eDBfieldMap *fmap) {
        dbresult *res = NULL;
        xmlDoc *ret = NULL;
        int fields;

        fields = eDBmappingFieldsPresent(fmap);
        if( (fields & (FIELD_UNAME | FIELD_CERTDIGEST | FIELD_REMOTEIP | FIELD_RECID)) == 0 ) {
                return eurephiaXML_ResultMsg(ctx, exmlERROR, NULL,
                                             "Missing username, IP address, certificate digest or blacklist ID");
        }

        res = sqlite_query_mapped(ctx, SQL_DELETE, "DELETE FROM openvpn_blacklist", NULL, fmap, NULL);
        if( sqlite_query_status(res) == dbSUCCESS ) {
                ret = eurephiaXML_ResultMsg(ctx, exmlRESULT, NULL, "Blacklisting removed");
        } else {
                xmlNode *err_n = NULL;

                eurephia_log(ctx, LOG_FATAL, 0, "Could not remove blacklisting");
                err_n = sqlite_log_error_xml(ctx, res);
                ret = eurephiaXML_ResultMsg(ctx, exmlERROR, err_n, "Failed to remove the blacklisting");
                xmlFreeNode(err_n);
        }
        sqlite_free_results(res);
        return ret;
}


/**
 * @copydoc eDBadminBlacklist()
 */
xmlDoc *eDBadminBlacklist(eurephiaCTX *ctx, xmlDoc *qryxml) {
        eDBfieldMap *fmap = NULL;
        char *mode = NULL;
        xmlDoc *resxml = NULL;
        xmlNode *root_n = NULL, *fieldmap_n = NULL;

        DEBUG(ctx, 20, "Function call: eDBadminBlacklist(ctx, {xmlDoc})");
        assert( (ctx != NULL) && (qryxml != NULL) );

        if( (ctx->context_type != ECTX_ADMIN_CONSOLE) && (ctx->context_type != ECTX_ADMIN_WEB) ) {
                eurephia_log(ctx, LOG_CRITICAL, 0,
                             "eurephia admin function call attempted with wrong context type");
                return NULL;
        }

        root_n = eurephiaXML_getRoot(ctx, qryxml, "blacklist", 1);
        if( root_n == NULL ) {
                eurephia_log(ctx, LOG_CRITICAL, 0, "Invalid XML input.");
                return NULL;
        }
        mode = xmlGetAttrValue(root_n->properties, "mode");
        if( mode == NULL ) {
                eurephia_log(ctx, LOG_ERROR, 0, "Missing mode attribute");
                return NULL;
        }

        fieldmap_n = xmlFindNode(root_n, "fieldMapping");
        if( fieldmap_n == NULL ) {
                eurephia_log(ctx, LOG_ERROR, 0, "Missing fieldMapping");
        }
        fmap = eDBxmlMapping(ctx, tbl_sqlite_blacklist, NULL, fieldmap_n);

        if( strcmp(mode, "list") == 0 ) {
                resxml = blacklist_list(ctx, fmap);
        } else if( strcmp(mode, "add") == 0 ) {
                resxml = blacklist_add(ctx, fmap);
        } else if( strcmp(mode, "delete") == 0 ) {
                resxml = blacklist_delete(ctx, fmap);
        } else {
                eurephia_log(ctx, LOG_ERROR, 0, "Blacklist - Unknown mode: '%s'", mode);
                resxml = eurephiaXML_ResultMsg(ctx, exmlERROR, NULL, "Unknown mode '%s'", mode);
        }
        eDBfreeMapping(fmap);
        return resxml;

}
