/* eurephiadb_struct.h  --  Database connection struct
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   eurephiadb_struct.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2008-11-05
 *
 * @brief  Definition of the eDBconn struct, a database independent
 *         connection handle
 *
 */

#ifndef   	EUREPHIADB_STRUCT_H_
# define   	EUREPHIADB_STRUCT_H_

#include "eurephia_values_struct.h"

/**
 *  Struct containing connection to the database we are using
 */
typedef struct {
        void *dbhandle;         /**< The DB handle used for the connection */
        char *dbname;           /**< String with name of database, used in logs */
        eurephiaVALUES *config; /**< eurephia config parameters retrieved from the database */
} eDBconn;

#endif
