/* eurephiadb_mapping.h  -- eurephiaDB table/field mapping
 *                          This mapping system, makes it possible
 *                          to access specific database table fields
 *                          through a unified API
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   eurephiadb_mapping.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2008-12-06
 *
 * @brief  Database field mapping between database and the C code/programs
 *
 * This feature makes it possible to let each database driver implementation to
 * use its own table and column/field names in the database, while having a
 * unified interface for table names and field names in the application.
 *
 * @remark The database may not have all assigned fields in the table as indicated here,
 *         as this is a way how to get around queries which uses JOIN between more tables.
 */

#ifndef   	EUREPHIADB_MAPPING_H
# define   	EUREPHIADB_MAPPING_H

/**
 *  Enumeration of different field types
 */
typedef enum eDBfieldType_e { ft_UNDEF, ft_INT, ft_STRING, ft_STRING_LOWER, ft_DATETIME, ft_PASSWD, ft_SETNULL, ft_BOOL } eDBfieldType;

/**
 *  Enumeration of different SQL filter types (IS NULL, =, !=,  <=, >=, >, <, etc)
 */
typedef enum eDBfieldFilterType_e { flt_NOTSET, flt_EQ, flt_NEQ, flt_LT, flt_LTE, flt_GT, flt_GTE } eDBfieldFilterType;

/**
 * Definition of the eDBfieldMap structure which maps a given numeric table ID and field ID
 * to a specific field name.  This structure can also holds values for these fields to be used
 * when doing SELECT, UPDATE or DELETE queries with WHERE sections and new values when doing
 * INSERT INTO and UPDATE
 */
typedef struct _eDBfieldMap_s {
        int tableid;                      /**< @see TABLE_USERS */
        char *table_alias;                /**< If the table needs an alias in the SQL query,
                                             it should be set here*/
        unsigned long long field_id;      /**< @see FIELD_NONE */
        eDBfieldType field_type;          /**< Defines what kind of data this field stores */
        eDBfieldFilterType filter_type;   /**< Only for WHERE sections, defines what kind of filter
                                               mode to use  */
        char *field_name;                 /**< Name of the field as a string */
        char *value;                      /**< Value of the field */
        struct _eDBfieldMap_s *next;      /**< Creates a pointer chain */
} eDBfieldMap;

extern const char *SESSION_STATUS[];

/**
 * TABLE ID definitions
 *
 * Gives each table needed for eurephia a numeric ID.  These IDs are sequential numbers, starting on 1
 * and being increased by 1.
 * @{
 */
#define TABLE_USERS          0x01
#define TABLE_CERTS          0x02
#define TABLE_USERCERTS      0x03
#define TABLE_LASTLOG        0x04
#define TABLE_ATTEMPTS       0x05
#define TABLE_BLACKLIST      0x06
#define TABLE_EUREPHIAADMACC 0x07
#define TABLE_FWPROFILES     0x08
#define TABLE_PLUGINS        0x09
/**
 * @}
 */

/**
 * FIELD ID definitions
 *
 * Gives each table needed for eurephia a numeric ID.  These IDs are increased by 2^x as these
 * fields can be processed in a bit-wise manner by some functions.
 * @{
 */
#define FIELD_NONE        0x00000000LL /**< Special field */
#define FIELD_RECID       0x00000001LL /**< Special field, should be used to indicate primary key of the table */
#define FIELD_UID         0x00000002LL
#define FIELD_CERTID      0x00000004LL
#define FIELD_UNAME       0x00000008LL
#define FIELD_ACTIVATED   0x00000010LL
#define FIELD_DEACTIVATED 0x00000020LL
#define FIELD_LASTACCESS  0x00000040LL

#define FIELD_CERTDEPTH   0x00000080LL
#define FIELD_CNAME       0x00000100LL
#define FIELD_EMAIL       0x00000200LL
#define FIELD_ORG         0x00000400LL
#define FIELD_REGISTERED  0x00000800LL

#define FIELD_REMOTEIP    0x00001000LL
#define FIELD_VPNIP       0x00002000LL
#define FIELD_ATTEMPTS    0x00004000LL
#define FIELD_LASTATTEMPT 0x00008000LL

#define FIELD_SESSTATUS   0x00010000LL
#define FIELD_LOGIN       0x00020000LL
#define FIELD_LOGOUT      0x00040000LL
#define FIELD_PASSWD      0x00080000LL
#define FIELD_CERTDIGEST  0x00100000LL

#define FIELD_ACCPROFILE  0x00200000LL

#define FIELD_INTERFACE   0x00400000LL
#define FIELD_ACCESSLVL   0x00800000LL

#define FIELD_MACADDR     0x01000000LL
#define FIELD_UICID       0x02000000LL

#define FIELD_DESCR       0x04000000LL
#define FIELD_FWPROFILE   0x08000000LL

#define FIELD_TYPE        0x10000000LL
#define FIELD_FILE        0x20000000LL
#define FIELD_CONFIG      0x40000000LL
#define FIELD_PLUGIN      0x80000000LL
/**
 * @}
 */


#ifdef EUREPHIADB_MAPPING_C

/**
 * Unified eurephia field names for the 'users' table
 */
static eDBfieldMap eTblMap_user[] = {
        {TABLE_USERS, NULL, FIELD_RECID,       ft_INT,      flt_EQ, "uid",         NULL, NULL},
        {TABLE_USERS, NULL, FIELD_UNAME,       ft_STRING,   flt_EQ, "username",    NULL, NULL},
        {TABLE_USERS, NULL, FIELD_PASSWD,      ft_PASSWD,   flt_EQ, "password",    NULL, NULL},
        {TABLE_USERS, NULL, FIELD_ACTIVATED,   ft_DATETIME, flt_EQ, "activated",   NULL, NULL},
        {TABLE_USERS, NULL, FIELD_DEACTIVATED, ft_DATETIME, flt_EQ, "deactivated", NULL, NULL},
        {TABLE_USERS, NULL, FIELD_LASTACCESS,  ft_DATETIME, flt_EQ, "lastaccess",  NULL, NULL},
        {0, NULL, 0, ft_UNDEF, flt_NOTSET, NULL, NULL, NULL}
};

/**
 * Unified eurephia field names for the 'certificates' table
 */
static eDBfieldMap eTblMap_certificates[] = {
        {TABLE_CERTS, NULL, FIELD_RECID,       ft_INT         , flt_EQ, "certid",      NULL, NULL},
        {TABLE_CERTS, NULL, FIELD_CERTDEPTH,   ft_INT         , flt_EQ, "depth",       NULL, NULL},
        {TABLE_CERTS, NULL, FIELD_CERTDIGEST,  ft_STRING_LOWER, flt_EQ, "digest",      NULL, NULL},
        {TABLE_CERTS, NULL, FIELD_CNAME,       ft_STRING      , flt_EQ, "cname",       NULL, NULL},
        {TABLE_CERTS, NULL, FIELD_ORG,         ft_STRING      , flt_EQ, "org",         NULL, NULL},
        {TABLE_CERTS, NULL, FIELD_EMAIL,       ft_STRING      , flt_EQ, "email",       NULL, NULL},
        {TABLE_CERTS, NULL, FIELD_REGISTERED,  ft_DATETIME    , flt_EQ, "registered",  NULL, NULL},
        {0, NULL, 0, ft_UNDEF, flt_NOTSET, NULL, NULL, NULL}
};

/**
 * Unified eurephia field names for the 'lastlog' table
 */
static eDBfieldMap eTblMap_lastlog[] = {
        {TABLE_LASTLOG, NULL, FIELD_UID,         ft_INT,      flt_EQ, "uid",       NULL, NULL},
        {TABLE_LASTLOG, NULL, FIELD_CERTID,      ft_INT,      flt_EQ, "certid",    NULL, NULL},
        {TABLE_LASTLOG, NULL, FIELD_REMOTEIP,    ft_STRING,   flt_EQ, "ip",        NULL, NULL},
        {TABLE_LASTLOG, NULL, FIELD_VPNIP,       ft_STRING,   flt_EQ, "vpnip",     NULL, NULL},
        {TABLE_LASTLOG, NULL, FIELD_SESSTATUS,   ft_STRING,   flt_EQ, "status",    NULL, NULL},
        {TABLE_LASTLOG, NULL, FIELD_LOGIN,       ft_DATETIME, flt_EQ, "login",     NULL, NULL},
        {TABLE_LASTLOG, NULL, FIELD_LOGOUT,      ft_DATETIME, flt_EQ, "logout",    NULL, NULL},
        {TABLE_LASTLOG, NULL, FIELD_RECID,       ft_INT,      flt_EQ, "id",        NULL, NULL},
        {TABLE_LASTLOG, NULL, FIELD_UNAME,       ft_STRING,   flt_EQ, "username",  NULL, NULL},
        {TABLE_LASTLOG, NULL, FIELD_MACADDR,     ft_STRING,   flt_EQ, "macaddr",   NULL, NULL},
        {TABLE_LASTLOG, NULL, FIELD_UICID,       ft_STRING,   flt_EQ, "uicid",     NULL, NULL},
        {0, NULL, 0, ft_UNDEF, flt_NOTSET, NULL, NULL, NULL}
};

/**
 * Unified eurephia field names for the 'attempts log' table
 */
static eDBfieldMap eTblMap_attempts[] = {
        {TABLE_ATTEMPTS, NULL, FIELD_UNAME,       ft_STRING      , flt_EQ, "username",    NULL, NULL},
        {TABLE_ATTEMPTS, NULL, FIELD_REMOTEIP,    ft_STRING      , flt_EQ, "ip",          NULL, NULL},
        {TABLE_ATTEMPTS, NULL, FIELD_CERTDIGEST,  ft_STRING_LOWER, flt_EQ, "digest",      NULL, NULL},
        {TABLE_ATTEMPTS, NULL, FIELD_ATTEMPTS,    ft_INT         , flt_EQ, "attempts",    NULL, NULL},
        {TABLE_ATTEMPTS, NULL, FIELD_REGISTERED,  ft_DATETIME    , flt_EQ, "registered",  NULL, NULL},
        {TABLE_ATTEMPTS, NULL, FIELD_LASTATTEMPT, ft_DATETIME    , flt_EQ, "lastattempt", NULL, NULL},
        {TABLE_ATTEMPTS, NULL, FIELD_RECID,       ft_INT         , flt_EQ, "id",          NULL, NULL },
        {0, NULL, 0, ft_UNDEF, flt_NOTSET, NULL, NULL, NULL}
};


/**
 * Unified eurephia field names for the 'blacklist' table
 */
static eDBfieldMap eTblMap_blacklist[] = {
        {TABLE_BLACKLIST, NULL, FIELD_UNAME,       ft_STRING      , flt_EQ, "username",     NULL, NULL},
        {TABLE_BLACKLIST, NULL, FIELD_REMOTEIP,    ft_STRING      , flt_EQ, "ip",           NULL, NULL},
        {TABLE_BLACKLIST, NULL, FIELD_CERTDIGEST,  ft_STRING_LOWER, flt_EQ, "digest",       NULL, NULL},
        {TABLE_BLACKLIST, NULL, FIELD_REGISTERED,  ft_DATETIME    , flt_EQ, "registered",   NULL, NULL},
        {TABLE_BLACKLIST, NULL, FIELD_LASTACCESS,  ft_DATETIME    , flt_EQ, "lastaccessed", NULL, NULL},
        {TABLE_BLACKLIST, NULL, FIELD_RECID,       ft_INT         , flt_EQ, "id",           NULL, NULL},
        {0, NULL, 0, ft_UNDEF, flt_NOTSET, NULL, NULL, NULL}
};


/**
 * Unified eurephia field names for the 'usercerts' table
 */
static eDBfieldMap eTblMap_usercerts[] = {
        {TABLE_USERCERTS, NULL, FIELD_UID,        ft_INT, flt_EQ,  "uid",            NULL, NULL},
        {TABLE_USERCERTS, NULL, FIELD_CERTID,     ft_INT, flt_EQ,  "certid",         NULL, NULL},
        {TABLE_USERCERTS, NULL, FIELD_ACCPROFILE, ft_INT, flt_EQ,  "accessprofile",  NULL, NULL},
        {TABLE_USERCERTS, NULL, FIELD_PLUGIN,     ft_INT, flt_EQ,  "authplugin",     NULL, NULL},
        {TABLE_USERCERTS, NULL, FIELD_UNAME,      ft_STRING,flt_EQ,"authusername",   NULL, NULL},
        {TABLE_USERCERTS, NULL, FIELD_REGISTERED, ft_INT, flt_EQ,  "registered",     NULL, NULL},
        {TABLE_USERCERTS, NULL, FIELD_RECID,      ft_INT, flt_EQ,  "uicid",          NULL, NULL},
        {0, NULL, FIELD_NONE, ft_UNDEF, flt_NOTSET, NULL, NULL, NULL}
};


/**
 * Unified eurephia field names for the 'eurephia_adminaccess' table
 */
static eDBfieldMap eTblMap_eurephiaadmacc[] = {
        {TABLE_EUREPHIAADMACC, NULL, FIELD_UID,       ft_INT,    flt_EQ, "uid",           NULL, NULL},
        {TABLE_EUREPHIAADMACC, NULL, FIELD_INTERFACE, ft_STRING, flt_EQ, "interface",     NULL, NULL},
        {TABLE_EUREPHIAADMACC, NULL, FIELD_ACCESSLVL, ft_STRING, flt_EQ, "accesslevel",   NULL, NULL},
        {0, NULL, FIELD_NONE, ft_UNDEF, flt_NOTSET, NULL, NULL, NULL}
};

/**
 * Unified eurephia field names for the 'accessprofiles' table
 */
static eDBfieldMap eTblMap_fwprofiles[] = {
        {TABLE_FWPROFILES,   NULL,  FIELD_DESCR,      ft_STRING      , flt_EQ, "description",   NULL, NULL},
        {TABLE_FWPROFILES,   NULL,  FIELD_FWPROFILE,  ft_STRING      , flt_EQ, "fwprofile",     NULL, NULL},
        {TABLE_FWPROFILES,   NULL,  FIELD_RECID,      ft_INT         , flt_EQ, "accessprofile", NULL, NULL},
        {TABLE_FWPROFILES,   NULL,  FIELD_UID,        ft_INT         , flt_EQ, "uid",           NULL, NULL},
        {TABLE_FWPROFILES,   NULL,  FIELD_UNAME,      ft_STRING      , flt_EQ, "username",      NULL, NULL},
        {TABLE_FWPROFILES,   NULL,  FIELD_CERTID,     ft_INT         , flt_EQ, "certid",        NULL, NULL},
        {TABLE_FWPROFILES,   NULL,  FIELD_CNAME,      ft_STRING      , flt_EQ, "cname",         NULL, NULL},
        {TABLE_FWPROFILES,   NULL,  FIELD_ORG,        ft_STRING      , flt_EQ, "org",           NULL, NULL},
        {TABLE_FWPROFILES,   NULL,  FIELD_EMAIL,      ft_STRING      , flt_EQ, "email",         NULL, NULL},
        {TABLE_FWPROFILES,   NULL,  FIELD_CERTDIGEST, ft_STRING_LOWER, flt_EQ, "digest",        NULL, NULL},
        {TABLE_FWPROFILES,   NULL,  FIELD_REGISTERED, ft_DATETIME, flt_EQ, "registered",    NULL, NULL},
        {0, NULL, FIELD_NONE, ft_UNDEF, flt_NOTSET, NULL, NULL, NULL}
};

/**
 * Unified eurephia field names for the 'plugins' table
 */
static eDBfieldMap eTblMap_plugins[] = {
        {TABLE_PLUGINS,   NULL,  FIELD_DESCR,      ft_STRING      , flt_EQ, "name",      NULL, NULL},
        {TABLE_PLUGINS,   NULL,  FIELD_TYPE,       ft_STRING      , flt_EQ, "type",      NULL, NULL},
        {TABLE_PLUGINS,   NULL,  FIELD_FILE,       ft_STRING      , flt_EQ, "dsofile",   NULL, NULL},
        {TABLE_PLUGINS,   NULL,  FIELD_CONFIG,     ft_STRING      , flt_EQ, "config",    NULL, NULL},
        {TABLE_PLUGINS,   NULL,  FIELD_ACTIVATED,  ft_BOOL        , flt_EQ, "enabled",   NULL, NULL},
        {TABLE_PLUGINS,   NULL,  FIELD_RECID,      ft_INT         , flt_EQ, "plugin_id", NULL, NULL},
        {0, NULL, FIELD_NONE, ft_UNDEF, flt_NOTSET, NULL, NULL, NULL}
};

#endif // #ifdef EUREPHIADB_MAPPING_C

#ifdef HAVE_LIBXML2
#include <libxml/tree.h>

void eDBfreeMapping(eDBfieldMap *p);
eDBfieldMap *eDBxmlMapping(eurephiaCTX *ctx, eDBfieldMap *dbmap, const char *tblalias, xmlNode *fmap_n);
const char *eDBmkSortKeyString(eDBfieldMap *tfmap, const char *skeys_str);
unsigned long long eDBmappingFieldsPresent(eDBfieldMap *map);
const char *eDBmappingGetValue(eDBfieldMap *map, long long field_id);
int eDBmappingSetValue(eDBfieldMap *map, long long field_id, char *value);

#endif      // HAVE_LIBXML2
#endif 	    // !EUREPHIADB_MAPPING_H
