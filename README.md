
eurephia - a flexible authentication and authorization plug-in for OpenVPN
==========================================================================

This project is an OpenVPN plug-in which enables both authentication and
access control of VPN users.

For more information, see the project web page:

      http://www.eurephia.net/



