/* eurephia_context.h  --  eurephiaCTX definition
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
*/

/**
 * @file   eurephia_context.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2008-08-06
 *
 * @brief  eurephia context structure definition.
 *
 */

#ifndef   	EUREPHIA_CONTEXT_H_
#define   	EUREPHIA_CONTEXT_H_

#include <stdio.h>
#include <eurephiadb_struct.h>
#include <eurephia_log_struct.h>
#include <eurephia_authplugin_context.h>

/**
 *  eurephia context types
 */
#define ECTX_NO_PRIVILEGES 0x1000 /**< The context should not have any privileges at all */
#define ECTX_PLUGIN_AUTH   0x1001 /**< The context is used in a openvpn plug-in setting */
#define ECTX_ADMIN_CONSOLE 0x2001 /**< The context is used via the eurephiadm console utility */
#define ECTX_ADMIN_WEB     0x2002 /**< The context is used via a web based utility */

#define SIZE_PWDCACHE_SALT 2048 /**< Defines the size of the in-memory password salt */

/**
 * OpenVPN tunnel types
 */
typedef enum OVPN_tunnelType_e { tuntype_UNKN, /**< Unknwown */
                                 tuntype_TAP,  /**< OpenVPN is used with --dev tap */
                                 tuntype_TUN   /**< OpenVPN is used with --dev tun */
} OVPN_tunnelType;


/**
 * main structure for the eurephia module context
 * - the same context structure is used for all OpenVPN sessions
 */
typedef struct {
        void *eurephia_driver;  /**< Pointer to the eurephia database drivers handler */
        void *eurephia_fw_intf; /**< Pointer to the eurephia firewall interface handler */
        OVPN_tunnelType tuntype;/**< Type of OpenVPN tunnel - TUN or TAP */
        eDBconn *dbc;           /**< Pointer to an eurephia database connection */
#ifdef EUREPHIA_FWINTF
        eurephiaFWINTF *fwcfg;  /**< Pointer to the firewall configuration */
#else
        void *fwcfg;            /**< Dummy pointer, if the firewall API is not enabled */
#endif
        unsigned int nointernalauth; /**< If set, don't use internal eurephia authentication */
        eAuthPluginCTX *authplugs; /**< Authentication plug-in contexts.  May be NULL */
        char *server_salt;      /**< The in-memory password salt, used for the password cache */
        eurephiaLOG *log;       /**< Log context, used by eurephia_log() */
        int fatal_error;        /**< If this flag is set, the execution should stop immediately */
        int context_type;       /**< Defines the context type */
        eurephiaVALUES *disconnected; /**< List over disconnected clients which has not yet been removed */
} eurephiaCTX;

#endif
