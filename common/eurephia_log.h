/* eurephia_log.h  --  eurephia logging module
 *
 *  GPLv2 only - Copyright (C) 2008 - 2013
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   eurephia_log.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2008-08-06
 *
 * @brief  Function for unified logging
 *
 */

#ifndef   	EUREPHIA_LOG_H_
#define   	EUREPHIA_LOG_H_

#include <stdarg.h>
#include "openvpn-plugin.h"
#include <eurephia_log_struct.h>
#include <eurephia_context.h>

#ifdef __GNUC__
/** Avoid GNU C compiler to complain about unused functions - only used where this is a false positive */
#define __CC_ATTR_USED__ __attribute__ ((used))
#else
/** For non GNU C compilers, make this just a NOOP */
#define __CC_ATTR_USED__
#endif

#ifdef ENABLE_DEBUG
#warning ######   DEBUG LOGGING IS ENABLED - THIS COULD BE A SECURITY ISSUE   ######
/**
 *  Wrapper function for DEBUG statements.  This is used to avoid adding debug code into the compiled
 *  binary if debug logging is not enabled at compile time.  This will always use the LOG_DEBUG target
 *  when calling eurephia_log().
 */
#define DEBUG(ctx, log_level, log_string...) _eurephia_log_func(ctx, LOG_DEBUG, log_level, __FILE__, __LINE__, ## log_string);
#else
#define DEBUG(ctx, lvl, rest...) {};
#endif

#ifdef SHOW_SECRETS
#warning ##########################################################################################
#warning ##                                                                                      ##
#warning ##  DEBUG LOGGING WITH SHOW_SECRETS IS ENABLED - THIS WILL LOG PASSWORDS IN CLEAR TEXT  ##
#warning ##                                                                                      ##
#warning ##########################################################################################
#endif

int eurephia_log_init(eurephiaCTX *ctx, const char *ident, const char *dest, int loglvl, plugin_vlog_t plglog_fnc);
void eurephia_log_close(eurephiaCTX *ctx);

/**
 * Simple log function which writes log data to the log file available in the eurephiaCTX
 * Frontend wrapper for the _eurephia_log_func() function.
 *
 * @param ctx eurephiaCTX
 * @param dst Log destination/priority (LOG_INFO, LOG_PANIC, LOG_ERROR, etc)
 * @param lvl Verbosity level of the message
 * @param log_string The message to be logged
 */
#define eurephia_log(ctx, dst, lvl, log_string...) _eurephia_log_func(ctx, dst, lvl, __FILE__, __LINE__, ## log_string)

/**
 * stdarg variaint of eurephia_log().  This takes the va_list ap and char *fmt pointers directly
 * Frontend wrapper for the _veurephia_log_func() function.
 *
 * @param ctx eurephiaCTX
 * @param dst Log destination/priority (LOG_INFO, LOG_PANIC, LOG_ERROR, etc)
 * @param lvl Verbosity level of the message
 * @param log_string The message to be logged
 */
#define veurephia_log(ctx, dst, lvl, va_ap, fmt) _veurephia_log_func(ctx, dst, lvl, __FILE__, __LINE__, va_ap, fmt)
void _veurephia_log_func(eurephiaCTX *ctx, int logdst, int loglvl, const char *file, int line,
                         va_list ap, const char *fmt);

/**
 * Internal function.  This function should normally be called via the eurephia_log() function.
 *
 * @param ctx    eurephiaCTX
 * @param logdst Log destination, can be LOG_INFO, LOG_DEBUG, LOG_WARNING, LOG_ERROR,
 *        LOG_CRITICAL, LOG_FATAL or LOG_PANIC
 * @param loglvl Log level of the message.  If the eurephiaCTX has a lower log level setup
 *        than what this parameter is set to, the message will not be logged.
 * @param file String containing file name of the place this function was called.  Usually the __FILE__ macro.
 * @param line Line number of the source file this function was called.  Usually the __LINE__ macro.
 * @param fmt    Contents of the log message (stdarg)
 *
 */
static void __CC_ATTR_USED__ _eurephia_log_func(eurephiaCTX *ctx, int logdst, int loglvl, const char *file, int line,
                        const char *fmt, ... )
{
        va_list ap;

        va_start(ap, fmt);
        _veurephia_log_func(ctx, logdst, loglvl, file, line, ap, fmt);
        va_end(ap);
}


#endif 	    /* !EUREPHIA_LOG_H_ */
