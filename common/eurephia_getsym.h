/* eurephia_getsym.h  --  Retrieves symbols from dlopened libraries
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   eurephia_getsym.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2008-08-10
 *
 * @brief  Helper functions for handling dynamic loaded objects (.so files)
 *
 */

#ifndef   	EUREPHIA_GETSYM_H_
#define   	EUREPHIA_GETSYM_H_

void *eGetSym_optional(eurephiaCTX *ctx, void *dlh, const char *symnam);
void *eGetSym(eurephiaCTX *ctx, void *dlh, const char *symnam);

#endif 	    /* !EUREPHIA_GETSYM_H_ */
