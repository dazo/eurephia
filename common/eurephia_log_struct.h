/* eurephia_log.h  --  eurephia log struct definition
 *
 *  GPLv2 only - Copyright (C) 2009 - 2013
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   eurephia_log_struct.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2009-09-23
 *
 * @brief  Defines the eurphia log structure
 *
 */

#ifndef EUREPHIA_LOG_STRUCT_H
#define EUREPHIA_LOG_STRUCT_H

#include <syslog.h>
#include "openvpn-plugin.h"

/*
 * A lot of these definitions are here to make sure LOG_* definitions will not
 * collide with syslog definitions.
 *
 */

#ifndef LOG_INFO
#define LOG_INFO  101		/**< Informational messages. Log level should be < 5 */
#endif

#ifndef LOG_DEBUG
#define LOG_DEBUG 102		/**< Messages intended when debugging. Only for log level > 10 */
#endif

#ifndef LOG_WARNING
#define LOG_WARNING 103		/**< Input data or processing revealed unexpected data. Log level never > 2*/
#endif

#ifndef LOG_ERR
#define LOG_ERR 104             /**< Alias for LOG_ERROR, in case it is not defined */
#endif
#define LOG_ERROR LOG_ERR	/**< API errors but not sever, program can continue to run */

#ifndef LOG_CRIT
#define LOG_CRIT 105            /**< Alias for LOG_CRITICAL */
#endif
#define LOG_CRITICAL LOG_CRIT	/**< Operation failed and might have been aborted.  Log level always 0 */

#ifndef LOG_ALERT
#define LOG_ALERT 106           /**< Alias for LOG_FATAL */
#endif
#define LOG_FATAL LOG_ALERT     /**< Operation failed and cannot continue. Log level always < 2 */

#ifndef LOG_EMERG
#define LOG_EMERG 107           /**< Alias for LOG_PANIC */
#endif
#define LOG_PANIC LOG_EMERG	/**< Action failed an program could not continue to run. Log level always 0 */


/**
 * Defines the different valid log types / logging backends
 */
typedef enum { logFILE,         /**< Log to file */
               logSYSLOG,       /**< Log via syslog */
               logOPENVPN,      /**< Log via OpenVPN's plugin v3 API */
} eurephiaLOGTYPE;

/**
 * eurephia log context.  Defines how the logging will be done when using eurephia_log()
 */
typedef struct {
        eurephiaLOGTYPE logtype; /**< Defines which log backend to use */
        unsigned int opened;     /**< Boolean flag, if the logging is openend and enabled */
        char *destination;       /**< String containing log destination info (filename, syslog facility) */
        plugin_vlog_t ovpn_log;  /**< OpenVPN plug-in v3 log API, used when logtype == logOPENVPN */

        FILE *logfile;           /**< File pointer to the log file if logtype == logFILE */
        int loglevel;            /**< Defines the log verbosity, higher number increases log verbosity */
} eurephiaLOG;

#endif
