/* environment.h  --  Function for extracting data from the OpenVPN environment table
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   environment.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2008-08-06
 *
 * @brief  Function for extracting data from the OpenVPN environment table.
 *
 *         A lot of the macros defined here are the preferred way how to access
 *         information from the environment table.  This is to make sure the same
 *         variable names are used and the same limitations to the value length
 *         is kept.
 *
 */

#ifndef _ENVIRONMENT_H
#define _ENVIRONMENT_H

/**
 * get_env() retrieve values from the openvpn environment table
 *
 * @param ctx        eurephiaCTX context
 * @param logmasking If 1, the value will be masked in the log files (eg. to hide password)
 * @param len        How many bytes to copy out of the environment variable
 * @param envp       the environment table
 * @param fmt        The key to look for (stdarg)
 *
 * @return Returns a const char * with the value, or NULL if not found
 */
char *get_env(eurephiaCTX *ctx, int logmasking, size_t len, const char *envp[], const char *fmt, ... );

#define MAXLEN_DEVNAME 64      /**< Maximum allowed length of the device name of the tunnel device */
/**
 * Macro for retrieving the OpenVPN tunnel device name (openvpn --dev option)
 *
 * @param ctx  eurephiaCTX
 * @param env  Char array pointer to the environment table where the value resides
 *
 * @return Returns a pointer to a new memory region with the value.  This region must be freed after use.
 * @see get_env()
 */
#define GETENV_DEVNAME(ctx, env) get_env(ctx, 0, MAXLEN_DEVNAME, env, "dev");

#define MAXLEN_DEVTYPE 8      /**< Maximum allowed length of the device type of the tunnel device */
/**
 * Macro for retrieving the OpenVPN tunnel device type (openvpn --dev-type option)
 *
 * @param ctx  eurephiaCTX
 * @param env  Char array pointer to the environment table where the value resides
 *
 * @return Returns a pointer to a new memory region with the value.  This region must be freed after use.
 * @see get_env()
 */
#define GETENV_DEVTYPE(ctx, env) get_env(ctx, 0, MAXLEN_DEVTYPE, env, "dev_type");

#define MAXLEN_TLSID 2048      /**< Maximum allowed length of the TLS ID string*/
/**
 * Macro for retrieving the TLS ID string of the clients certificate
 *
 * @param ctx  eurephiaCTX
 * @param env  Char array pointer to the environment table where the value resides
 * @param id   Which TLS ID to retrieve. 0 is the clients certificate, >=1 are CA certificates.
 *
 * @return Returns a pointer to a new memory region with the value.  This region must be freed after use.
 * @see get_env()
 */
#define GETENV_TLSID(ctx, env, id) get_env(ctx, 0, MAXLEN_TLSID, env, "tls_id_%i", id)

#define MAXLEN_TLSDIGEST 60    /**< Maximum allowed length of the certificate digest/fingerprint*/
/**
 * Macro for retrieving the certificate digest/fingerprint
 *
 * @param ctx  eurephiaCTX
 * @param env  Char array pointer to the environment table where the value resides
 * @param id   Which TLS digest to retrieve. 0 is the clients certificate, >=1 are CA certificates.
 *
 * @return Returns a pointer to a new memory region with the value.  This region must be freed after use.
 * @see get_env()
 */
#define GETENV_TLSDIGEST(ctx, env, id) get_env(ctx, 0, MAXLEN_TLSDIGEST, env, "tls_digest_%i", id)

#define MAXLEN_UNTRUSTEDIP 34  /**< Maximum allowed length of the untrusted public IP address of the client*/
/**
 * Macro for retrieving the IP address of the OpenVPN client.  The untrusted IP is available before the
 * client has been authenticated.
 *
 * @param ctx  eurephiaCTX
 * @param env  Char array pointer to the environment table where the value resides
 *
 * @return Returns a pointer to a new memory region with the value.  This region must be freed after use.
 * @see get_env()
 */
#define GETENV_UNTRUSTEDIP(ctx, env) get_env(ctx, 0, MAXLEN_UNTRUSTEDIP, env, "untrusted_ip")

#define MAXLEN_USERNAME 34     /**< Maximum allowed length of the username*/
/**
 * Macro for retrieving the username the OpenVPN client wants to authenticate her/himself as.
 *
 * @param ctx  eurephiaCTX
 * @param env  Char array pointer to the environment table where the value resides
 *
 * @return Returns a pointer to a new memory region with the value.  This region must be freed after use.
 * @see get_env()
 */
#define GETENV_USERNAME(ctx, env) get_env(ctx, 0, MAXLEN_USERNAME, env, "username")

#define MAXLEN_PASSWORD 64     /**< Maximum allowed length of the password*/
/**
 * Macro for retrieving the password the OpenVPN client uses for the authentication.
 *
 * @param ctx  eurephiaCTX
 * @param env  Char array pointer to the environment table where the value resides
 *
 * @return Returns a pointer to a new memory region with the value.  This region must be freed after use.
 * @see get_env()
 */
#define GETENV_PASSWORD(ctx, env) get_env(ctx, 1, MAXLEN_PASSWORD, env, "password")

#define MAXLEN_BYTESRECEIVED 21  /**< Maximum allowed length of the received bytes value*/
/**
 * Macro for retrieving the number of bytes the OpenVPN server has received from the client.  This
 * is only available when the OpenVPN decides to disconnect the client session.
 *
 * @param ctx  eurephiaCTX
 * @param env  Char array pointer to the environment table where the value resides
 *
 * @return Returns a pointer to a new memory region with the value.  This region must be freed after use.
 * @see get_env()
 */
#define GETENV_BYTESRECEIVED(ctx, env) get_env(ctx, 0, MAXLEN_BYTESRECEIVED, env, "bytes_received");

#define MAXLEN_BYTESSENT 21    /**< Maximum allowed length of the sent bytes value*/
/**
 * Macro for retrieving the number of bytes the OpenVPN server has sent to the client.  This
 * is only available when the OpenVPN decides to disconnect the client session.
 *
 * @param ctx  eurephiaCTX
 * @param env  Char array pointer to the environment table where the value resides
 *
 * @return Returns a pointer to a new memory region with the value.  This region must be freed after use.
 * @see get_env()
 */
#define GETENV_BYTESSENT(ctx, env) get_env(ctx, 0, MAXLEN_BYTESSENT, env, "bytes_sent");

#define MAXLEN_TIMEDURATION 21 /**< Maximum allowed length of the session time value*/
/**
 * Macro for retrieving the number of seconds the OpenVPN session lasted.  This
 * is only available when the OpenVPN decides to disconnect the client session.
 *
 * @param ctx  eurephiaCTX
 * @param env  Char array pointer to the environment table where the value resides
 *
 * @return Returns a pointer to a new memory region with the value.  This region must be freed after use.
 * @see get_env()
 */
#define GETENV_TIMEDURATION(ctx, env) get_env(ctx, 0, MAXLEN_TIMEDURATION, env, "time_duration");

#define MAXLEN_POOLIPADDR 34   /**< Maximum allowed length of the clients VPN IP address*/
/**
 * Macro for retrieving the the IP address of the tunnelled VPN network for the current session.
 *
 * @param ctx  eurephiaCTX
 * @param env  Char array pointer to the environment table where the value resides
 *
 * @return Returns a pointer to a new memory region with the value.  This region must be freed after use.
 * @see get_env()
 */
#define GETENV_POOLIPADDR(ctx, env) get_env(ctx, 0, MAXLEN_POOLIPADDR, env, "ifconfig_pool_remote_ip");

#define MAXLEN_TRUSTEDIP 34    /**< Maximum allowed length of the trusted public IP of the client*/
/**
 * Macro for retrieving the OpenVPN clients public IP address.  This is available after the client
 * has authenticatied successfully
 *
 * @param ctx  eurephiaCTX
 * @param env  Char array pointer to the environment table where the value resides
 *
 * @return Returns a pointer to a new memory region with the value.  This region must be freed after use.
 * @see get_env()
 */
#define GETENV_TRUSTEDIP(ctx, env) get_env(ctx, 0, MAXLEN_TRUSTEDIP, env, "trusted_ip");

#define MAXLEN_PROTO1 4        /**< Maximum allowed length of protocol value (tcp/udp)*/
/**
 * Macro for retrieving the protocol the user is using.  Normally, this value will be 'udp' or 'tcp'
 *
 * @param ctx  eurephiaCTX
 * @param env  Char array pointer to the environment table where the value resides
 *
 * @return Returns a pointer to a new memory region with the value.  This region must be freed after use.
 * @see get_env()
 */
#define GETENV_PROTO1(ctx, env) get_env(ctx, 0, MAXLEN_PROTO1, env, "proto_1");

#define MAXLEN_CNAME 64        /**< Maximum allowed length of X.509 Common Name field*/
/**
 * Macro for retrieving the X.509 Common Name field from the clients certificate.
 *
 * @param ctx  eurephiaCTX
 * @param env  Char array pointer to the environment table where the value resides
 *
 * @return Returns a pointer to a new memory region with the value.  This region must be freed after use.
 * @see get_env()
 */
#define GETENV_CNAME(ctx, env) get_env(ctx, 0, MAXLEN_CNAME, env, "common_name");

#define MAXLEN_TRUSTEDPORT 6   /**< Maximum allowed length of the clients OpenVPN port */
/**
 * Macro for retrieving the port a authenticated OpenVPN client is connecting from.
 *
 * @param ctx  eurephiaCTX
 * @param env  Char array pointer to the environment table where the value resides
 *
 * @return Returns a pointer to a new memory region with the value.  This region must be freed after use.
 * @see get_env()
 */
#define GETENV_TRUSTEDPORT(ctx, env) get_env(ctx, 0, MAXLEN_TRUSTEDPORT, env, "trusted_port");

#define MAXLEN_UNTRUSTEDPORT 6 /**< Maximum allowed length of the clients OpenVPN port*/
/**
 * Macro for retrieving the port a unauthenticated OpenVPN client is connecting from.
 *
 * @param ctx  eurephiaCTX
 * @param env  Char array pointer to the environment table where the value resides
 *
 * @return Returns a pointer to a new memory region with the value.  This region must be freed after use.
 * @see get_env()
 */
#define GETENV_UNTRUSTEDPORT(ctx, env) get_env(ctx, 0, MAXLEN_UNTRUSTEDPORT, env, "untrusted_port");

#define MAXLEN_DAEMON 32       /**< Maximum allowed length of the daemon configuration value*/
/**
 * Macro for retrieving a configuration parameter, which defines if the OpenVPN server
 * started as a daemon.
 *
 * @param env  Char array pointer to the environment table where the value resides
 *
 * @return Returns a pointer to a new memory region with the value.  This region must be freed after use.
 * @see get_env()
 */
#define GETENV_DAEMON(env) get_env(NULL, 0, MAXLEN_DAEMON, env, "daemon");

#define MAXLEN_DAEMONLOGREDIR 32 /**< Maximum allowed length of daemon_log_redirect config value*/
/**
 * Macro for retrieving a configuration parameter, which defines if the OpenVPN server
 * is redirecting the logs or not.
 *
 * @param env  Char array pointer to the environment table where the value resides
 *
 * @return Returns a pointer to a new memory region with the value.  This region must be freed after use.
 * @see get_env()
 */
#define GETENV_DAEMONLOGREDIR(env) get_env(NULL, 0, MAXLEN_DAEMONLOGREDIR, env, "daemon_log_redirect");

#endif
