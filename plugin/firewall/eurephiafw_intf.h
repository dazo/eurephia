/* eurephiafw_intf.h  --  Driver interface for the firewall module
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   eurephiafw_intf.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2008-08-10
 *
 * @brief  Declaration of the firewall driver API
 *
 */


#ifndef   	EUREPHIAFW_INTF_H_
#define   	EUREPHIAFW_INTF_H_

#include <eurephia_driverapi.h>

/**
 * Mandatory function, contains driver information.
 *
 * @return Should retun a static string, containing the version information.
 */
const char * EUREPHIA_DRIVERAPI_FUNC(eFWinterfaceVersion)(void);

/**
 * Mandatory function, contains driver information.
 *
 * @return Should retun an integer which correponds to the API level the interface driver uses.
 */
int EUREPHIA_DRIVERAPI_FUNC(eFWinterfaceAPIversion)(void);

/**
 * The main routine of the firewall interface.  This function should not return before
 * the firewall interface is supposed to shut down.
 *
 * @param fwargs efw_threaddata pointer, with needed information to communicate with the openvpn process.
 */
void EUREPHIA_DRIVERAPI_FUNC(eFW_RunFirewall)(void *fwargs);

#endif 	    /* !EUREPHIAFW_INTF_H_ */
