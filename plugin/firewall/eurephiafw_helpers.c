/* eurephiafw_helpers.c  --  Helper functions, shared between main module and
 *                           firewall module.  Setting up POSIX MQ and semaphores
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   eurephiafw_helpers.c
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2009-08-14
 *
 * @brief  Helper functions which is shared between the main eurephia-auth module and
 *         the firewall module.  It takes care of preparing POSIX MQ queues and semaphores.
 *
 */

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <semaphore.h>
#include <mqueue.h>

#define EUREPHIA_FWINTF
#include <eurephiafw_struct.h>
#include <eurephia_context.h>
#include "eurephia_log.h"
#include "eurephiafw.h"
#include "eurephiafw_helpers.h"

/**
 * Prepares the POSIX Semaphores used to control the communication between the openvpn process
 * and the firewall updater process.
 *
 * @param ctx eurephiaCTX
 * @param cfg efw_threaddata, data used in the firewall process and contains pointers to the semaphores.
 *
 * @return Returns 1 on success, otherwise 0.
 */
int efwSetupSemaphores(eurephiaCTX *ctx, efw_threaddata *cfg) {
        // Initialise the main process' semaphore
        cfg->semp_master = sem_open(SEMPH_MASTER, O_CREAT, 0666, 0);
        if( cfg->semp_master == SEM_FAILED ) {
                eurephia_log(ctx, LOG_FATAL, 0, "Could not setup semaphore for eFW master: %s", strerror(errno));
                return 0;
        }

        // Initialise the worker process' semaphore
        cfg->semp_worker = sem_open(SEMPH_WORKER, O_CREAT, 0666, 0);
        if( cfg->semp_worker == SEM_FAILED ) {
                eurephia_log(ctx, LOG_FATAL, 0, "Could not setup semaphore for eFW worker: %s", strerror(errno));
                return 0;
        }
        return 1;
}


/**
 * Removes the semaphores created by efwSetupSemaphores().
 *
 * @param ctx eurephiaCTX
 * @param cfg efw_threaddata, data used in the firewall process and contains pointers to the semaphores.
 *
 * @return Returns 1 on success, otherwise 0.
 */
int efwRemoveSemaphores(eurephiaCTX *ctx, efw_threaddata *cfg) {
        if( sem_close(cfg->semp_worker) != 0 ) {
                eurephia_log(ctx, LOG_WARNING, 0,
                             "eFW: Could not destroy semaphore for worker: %s", strerror(errno));
        }
        sem_unlink(SEMPH_WORKER);

        if( sem_close(cfg->semp_master) != 0 ) {
                eurephia_log(ctx, LOG_WARNING, 0,
                             "eFW: Could not destroy semaphore for master: %s", strerror(errno));
        }
        sem_unlink(SEMPH_MASTER);
        return 1;
}


/**
 * Creates the needed POSIX MQ message queues.
 *
 * @param ctx eurephiaCTX
 * @param cfg efw_threaddata, data used in the firewall process and contains pointers to the MQ fd.
 *
 * @return Returns 1 on success, otherwise 0.
 */
int efwSetupMessageQueue(eurephiaCTX *ctx, efw_threaddata *cfg) {
        struct mq_attr mqattr;

        // Prepare a POSIX Message Queue
        mqattr.mq_maxmsg = 10;
        mqattr.mq_msgsize = EFW_MSG_SIZE;
        mqattr.mq_flags = 0;
        cfg->msgq = mq_open(MQUEUE_NAME, O_RDWR | O_CREAT, 0600, &mqattr);
        if( cfg->msgq < 0 ) {
                eurephia_log(ctx, LOG_FATAL, 0, "Could not open message queue: %s", strerror(errno));
                return 0;
        }
        return 1;
}


/**
 * Removes the POSIX MQ message queues created by efwSetupMessageQueue()
 *
 * @param ctx eurephiaCTX
 * @param cfg efw_threaddata, data used in the firewall process and contains pointers to the MQ fd.
 *
 * @return Returns 1 on success, otherwise 0.
 */
int efwRemoveMessageQueue(eurephiaCTX *ctx, efw_threaddata *cfg) {
        // Close and remove the message queue used
        if( mq_close((*cfg).msgq) != 0 ) {
                eurephia_log(ctx, LOG_WARNING, 0, "Could not do close the message queue used for eFW: %s",
                             strerror(errno));
        }

        if( mq_unlink(MQUEUE_NAME) != 0 ) {
                eurephia_log(ctx, LOG_WARNING, 0, "Could not do close the message queue used for eFW: %s",
                             strerror(errno));
        }
        return 1;
}
