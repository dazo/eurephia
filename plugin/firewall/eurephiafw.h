/* eurephiafw.h  --  Firewall interface loader for the eurephia module
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   eurephiafw.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2008-08-10
 *
 * @brief  Takes care of loading the configured firewall driver and provides a
 *         generic API for updating the firewall rules.
 *
 */

#ifndef   	EUREPHIAFW_H_
#define   	EUREPHIAFW_H_

int eFW_load(eurephiaCTX *ctx, const char *intf);
int eFW_unload(eurephiaCTX *ctx);

void eFW_StartFirewall(eurephiaCTX *ctx, const int daemon, const int logredir);
void eFW_StopFirewall(eurephiaCTX *ctx);
int eFW_UpdateFirewall(eurephiaCTX *ctx, eFWupdateRequest *request);

#endif 	    /* !EUREPHIAFW_H_ */
