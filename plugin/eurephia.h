/* eurephia.h  --  Main API for the eurephia authentication module
 *
 *  GPLv2 only - Copyright (C) 2008 - 2013
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   eurephia.h
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2008-08-06
 *
 * @brief  The core eurephia functions which is called from OpenVPN.
 *
 */

#ifndef   	EUREPHIA_H_
#define   	EUREPHIA_H_

#include <stdarg.h>

char *get_env(eurephiaCTX *ctx, int logmasking, size_t len, const char *envp[],
              const char *fmt, ... );

eurephiaCTX *eurephiaInit(const char const **argv, const char const **envp,
                          struct openvpn_plugin_callbacks const *ovpn_callbacks);
int eurephiaShutdown(eurephiaCTX *ctx);

int eurephia_tlsverify(eurephiaCTX *ctx, const char **argv, const char *depth, certinfo *ci);
int eurephia_userauth(eurephiaCTX *ctx, const char **env, certinfo *ci);
int eurephia_connect(eurephiaCTX *ctx, const char **env, certinfo *ci);
int eurephia_disconnect(eurephiaCTX *ctx, const char **env, certinfo *ci);
int eurephia_learn_address(eurephiaCTX *ctx, const char *mode, const char *macaddr,
                           const char **env, certinfo *ci);

#endif 	    /* !EUREPHIA_H_ */
