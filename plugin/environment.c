/* environment.c  --  Function for extracting data from the OpenVPN environment table
 *
 *  GPLv2 only - Copyright (C) 2008 - 2012
 *               David Sommerseth <dazo@users.sourceforge.net>
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; version 2
 *  of the License.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * @file   environment.c
 * @author David Sommerseth <dazo@users.sourceforge.net>
 * @date   2008-08-06
 *
 * @brief  Function for extracting data from the OpenVPN environment table.
 *
 */

#include <stdio.h>
#include <stdarg.h>
#include <string.h>

#include <eurephia_nullsafe.h>
#include <eurephia_context.h>
#include <eurephia_log.h>

/**
 * get_env() retrieve values from the openvpn environment table
 *
 * @param ctx        eurephiaCTX context
 * @param logmasking If 1, the value will be masked in the log files (eg. to hide password)
 * @param len        How many bytes to copy out of the environment variable
 * @param envp       the environment table
 * @param fmt        The key to look for (stdarg)
 *
 * @return Returns a const char * with the value, or NULL if not found
 */
char *get_env(eurephiaCTX *ctx, int logmasking, size_t len,
              const char *envp[], const char *fmt, ... )
{
        if (envp) {
                va_list ap;
                char key[384];
                int keylen = 0;
                int i;

                // Build up the key we are looking for
                memset(&key, 0, 384);
                va_start(ap, fmt);
                vsnprintf(key, 382, fmt, ap);

                // Find the key
                keylen = strlen (key);
                for (i = 0; envp[i]; ++i) {
                        if (!strncmp (envp[i], key, keylen)) {
                                const char *cp = envp[i] + keylen;
                                char *ret = NULL;
                                if (*cp == '=') {
                                        ret = malloc_nullsafe(ctx, len+2);
                                        strncpy(ret, cp+1, len);
#ifdef ENABLE_DEBUG
                                        int do_mask = 0;
#ifndef SHOW_SECRETS
                                        do_mask = logmasking;
#endif
                                        if( ctx != NULL ) {
                                                DEBUG(ctx, 30, "Function call: get_env(envp, '%s') == '%s'",
                                                      key, (do_mask == 0 ? ret : "xxxxxxxxxxxxxx"));
                                        }
#endif
                                        return ret;
                                }
                        }
                }
                if( ctx != NULL ) {
                        DEBUG(ctx, 15, "Function call: get_env(envp, '%s') -- environment variable not found",
                              key);
                }
                va_end(ap);
        }
        return NULL;
}
